﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO.Ports;
using System.IO;
using System.Xml;
using System.Xml.Linq;

using System.Threading;
using System.Runtime.InteropServices;


using System.Globalization;

using System.Net;

using GMap.NET;
using GMap.NET.WindowsForms;
using GMap.NET.WindowsForms.Markers;

using System.Net;
using System.Net.Sockets;

namespace HarpyWeatherBalloon
{
    public partial class Form1 : Form
    {
        public struct ZondMessageType
        {
            public DateTime Time;

            public int SecondFromStart;

            public int StartPriznak;
            public int ZondId;
            public int Num;
            public int State;
            public double P;
            public double T;
            public double U;
            public int Reserv;

            public int H_m;
            public double Altitude;

            public int Lat;
            public double Latitude;
            
            public int Lon;
            public double Longitude;
            
            public double Distance;

            public int Dir;
            public double Direct;

            public int Sp;
            public double Speed;

            public int Fr;
            public double Frequency;


            public int Temp;
            public double Temperature;
            public int Hum;
            public double Humidity;


            public double RSSI;

            public double E;
            public double A;

            public int CRC;
            public ushort CRC16;
            public uint CRC16uint;

        }

        ZondMessageType[] ZondMessage;

        public double P_Demo;
        public double T_Demo;
        public double U_Demo;
        public double Longitude_Demo;
        public double Latitude_Demo;
        public double Altitude_Demo;
        public double Temperature_Demo;
        public double Humidity_Demo;
        public double Distance_Demo;
        
        public string FN_LogZond;

        public struct SerialPostSettings
        {
            public StopBits StopBits;
            public int DataBits;
            public int BaudRate;
            public string PortName;
            public Parity Parity;
            public int ZondPeriodSend;
        }

        public string DatasFileName;
        public string DatasFileInfo;
        public string DatasFileHexName;
        public string DatasFileRaw;
        public string DatasFileTu;
        public string DatasFileCrd;
        public DateTime DTStart;

        public string ResultFolder;

        public struct StationParametrsType
        {
            public string StationName;
            public int SynopIndex;
            public double Longitude;
            public double Latitude;
            public double Altitude;
        }

        public struct SendDatasParametrsType
        {
            public bool SendDatas;
            public string URL;
        }
        
        public struct AMK_SettingsType
        {
            public string IP_adress;
            public string IP_port;
        }

        public struct InfoDatasType
        {
            public string OnGroundPressure;
            public string OnGroundWindDirection;
            public string OnGroundWindVelocity;
            public string OnGroundHumidityError;
            public string OnGroundTemperatureError;
            public string NebulosityCode;
            public string ZondType;
        }

        public struct ParamType
        {
            public string N;
            public string Val;
        }

        public struct Admissible_errorType
        {
            public string temp;
            public string hum;
        }

        

        InfoDatasType InfoDatas;

        StationParametrsType StationParametrs;

        SendDatasParametrsType SendDatasParametrs;
        AMK_SettingsType AMK_TCPIP;
        Admissible_errorType Admissible_error;

        public SerialPostSettings ZondSerialPort;

        private FormAbout f2 = new FormAbout();

        private FormSettings f3 = new FormSettings();

        public DateTime Lastmessagetime;
        public TimeSpan delta_Lastmessagetime;
        public int MessageCount;
        public byte[] ostatokvportu;
        public double[] Altitude3point;
        

        public byte[] buffer_start;
        public byte[] buffer_end;
        public byte priznak_part_of_send;

        Thread t = null;
        Thread t_map = null;
        Thread t_zond = null;
        Thread t_check = null;

        Thread t_dTime = null;

        Thread t_amk = null;

        bool testStart = false;
        bool Stopflag = true;
        private static TcpClient client;
        static string responseData = String.Empty;
        static Boolean QML_connection = false;

        static Boolean dataRead = false;

        private static DateTime TimePreviousRead;
        private static int WaitTimeRead;

        int a_byte;

        double msec;
        DateTime dt;
        DateTime dt1;
        DateTime time_now;
        byte[] bufferOnline = new byte[35];
        int counter;

        bool com_connection;
        public static string responseQMLString = String.Empty;

        ParamType[] QMLStringParam; 



        public const int WM_USER = 0x0400;
        static string s;
        double lon;
        double lat;
        private static IntPtr hww;

        public struct PointsType
        {
            public string ID;
            public string Name;
            public double Lat;
            public double Lon;
            public GMapMarker marker;
            public GMapPolygon Polyg;
        }


        public PointsType[] Points;

        public GMapOverlay markersOverlay = new GMapOverlay("markers");

        public Form1()
        {
            InitializeComponent();
            SendDatasParametrs.URL = "";
            hww = Handle;
        }

        uint CRC16_2(byte[] buf, int len)
        {
            uint crc = 0xFFFF;

            for (int pos = 0; pos < len; pos++)
            {
                crc ^= (uint)buf[pos];          // XOR byte into least sig. byte of crc

                for (int i = 8; i != 0; i--)
                {    // Loop over each bit
                    if ((crc & 0x0001) != 0)
                    {      // If the LSB is set
                        crc >>= 1;                    // Shift right and XOR 0xA001
                        crc ^= 0xA001;
                    }
                    else                            // Else LSB is not set
                        crc >>= 1;                    // Just shift right
                }
            }
            // Note, this number has low and high bytes swapped, so use it accordingly (or swap bytes)
            return crc;
        }

        public static ushort CRC16_alg(byte[] msg)
        {
            const ushort polinom = 0xa001;
            
            ushort code = 0xffff;

            for (int i = 0, size = msg.Length; i < size; ++i)
            {
                code ^= (ushort)(msg[i] << 8);

                for (uint j = 0; j < 8; ++j)
                {
                    code >>= 1;
                    if ((code & 0x01) != 0) code ^= polinom;
                }
            }

            return code;
        }

        public bool ReadZondDemo()
        {
            P_Demo = -999;
            T_Demo = -999;
            U_Demo = -999;
            Longitude_Demo = -999;
            Latitude_Demo = -999;
            Altitude_Demo = -999;
            Temperature_Demo = -999;
            Humidity_Demo = -999;
            Distance_Demo = -999;

            bool res = false;   
            string FN_Zond_Demo = System.AppDomain.CurrentDomain.BaseDirectory;
            FN_Zond_Demo = System.AppDomain.CurrentDomain.BaseDirectory + "Zond_Demo.xml";

            string XMLZond_Demo = System.AppDomain.CurrentDomain.BaseDirectory + "Zond_Demo.xml";

            try
            {
                StreamReader XMLStream = new StreamReader(FN_Zond_Demo);

                XMLZond_Demo = XMLStream.ReadToEnd();

                XMLStream.Close();

                XElement xmlTree = XElement.Parse(XMLZond_Demo);

                XName XDemo = "Demo";

                XName XP = "P";
                XName XT = "T";
                XName XU = "U";
                XName XLongitude = "Longitude";
                XName XLatitude = "Latitude";
                XName XAltitude = "Altitude";
                XName XTemperature = "Temperature";
                XName XHumidity = "Humidity";
                XName XDistance = "Distance";

                try
                {

                    string ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                    try
                    {

                        string s;

                        s = xmlTree.Element(XP).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XP).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            P_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XT).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XT).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            T_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XU).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XU).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            U_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XLongitude).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XLongitude).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Longitude_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XLatitude).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XLatitude).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Latitude_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XAltitude).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XAltitude).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Altitude_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XTemperature).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XTemperature).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Temperature_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XHumidity).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XHumidity).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Humidity_Demo = Convert.ToDouble(s);
                        }

                        s = xmlTree.Element(XDistance).Attribute(XDemo).Value;
                        if (s == "true")
                        {
                            s = xmlTree.Element(XDistance).Value;
                            if (ds == ".")
                                s = s.Replace(",", ".");
                            else if (ds == ",")
                                s = s.Replace(".", ",");
                            Distance_Demo = Convert.ToDouble(s);
                        }
                    }
                    catch
                    {
                        res = false;
                    }
                    res = true;
                }
                catch
                {
                    res = false;
                }
            }
            catch
            {
                res = false;
            }

            return res;
        }
        public bool ReadZondSettigs()
        {
            bool res = false;
            int i;
            string FN_Zond_Settings = System.AppDomain.CurrentDomain.BaseDirectory;
            FN_LogZond = System.AppDomain.CurrentDomain.BaseDirectory + "Zond.log";

            FN_Zond_Settings = System.AppDomain.CurrentDomain.BaseDirectory + "Zond_Settings.xml";

            string XMLZond_Settings = System.AppDomain.CurrentDomain.BaseDirectory + "Zond_Settings.xml";

            StreamReader XMLStream = new StreamReader(FN_Zond_Settings);

            XMLZond_Settings = XMLStream.ReadToEnd();

            XMLStream.Close();

            XElement xmlTree = XElement.Parse(XMLZond_Settings);

            XName XSerialPort_Settings = "SerialPort_Settings";
            XName XStation_Settings = "Station_Settings";
            XName XSendDatas_Settings = "SendDatas_Settings";
            XName XАМК_Settings = "АМК_Settings";
            XName XAdmissible_error = "Admissible_error";

            XName XStationName = "StationName";
            XName XSynopIndex = "SynopIndex";
            XName XLongitude = "Longitude";
            XName XLatitude= "Latitude";
            XName XAltitude = "Altitude";

            XName XPortName = "PortName";
            XName XStopBits = "StopBits";
            XName XDataBits = "DataBits";
            XName XBaudRate = "BaudRate";
            XName XParity = "Parity";
            XName XZondPeriodSend = "ZondPeriodSend";

            XName XSendToURL = "SendToURL";
            XName XURL = "URL";

            XName Xip_adress = "ip_adress";
            XName Xip_port = "ip_port";

            XName Xtemperature = "temperature";
            XName Xhumidity = "humidity";

            try
            {

                string ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                                try
                {
                ZondSerialPort.PortName = xmlTree.Element(XSerialPort_Settings).Element(XPortName).Value;
                // ZondSerialPort.StopBits = xmlTree.Element(XSerialPort_Settings).Element(XStopBits).Value;
                ZondSerialPort.StopBits = StopBits.One;
               // ZondSerialPort.DataBits = Convert.ToInt32(xmlTree.Element(XSerialPort_Settings).Element(XDataBits).Value);
                ZondSerialPort.BaudRate = Convert.ToInt32(xmlTree.Element(XSerialPort_Settings).Element(XBaudRate).Value);
                ZondSerialPort.DataBits = 8;
                // SerialPort_Parity = xmlTree.Element(XSerialPort_Settings).Element(XParity).Value;
                ZondSerialPort.ZondPeriodSend = Convert.ToInt32(xmlTree.Element(XSerialPort_Settings).Element(XZondPeriodSend).Value);
                ZondSerialPort.Parity = Parity.None;


                }
                                catch
                                {
                                }
                try
                {
                    StationParametrs.StationName = xmlTree.Element(XStation_Settings).Element(XStationName).Value;
                    StationParametrs.SynopIndex = Convert.ToInt32(xmlTree.Element(XStation_Settings).Element(XSynopIndex).Value);

                string s = xmlTree.Element(XStation_Settings).Element(XAltitude).Value;
                if (ds == ".")
                    s=s.Replace(",", ".");
                else if (ds == ",")
                    s=s.Replace(".", ",");
                StationParametrs.Altitude = Convert.ToDouble(s);
                
                s = xmlTree.Element(XStation_Settings).Element(XLatitude).Value;
                if (ds == ".")
                    s = s.Replace(",", ".");
                else if (ds == ",")
                    s = s.Replace(".", ",");
                StationParametrs.Latitude = Convert.ToDouble(s);
                
                s = xmlTree.Element(XStation_Settings).Element(XLongitude).Value;
                if (ds == ".")
                    s = s.Replace(",", ".");
                else if (ds == ",")
                    s = s.Replace(".", ",");
                StationParametrs.Longitude = Convert.ToDouble(s);

                }
                catch
                {
                }

                try
                {
                    SendDatasParametrs.URL = xmlTree.Element(XSendDatas_Settings).Element(XURL).Value.ToString();
                    SendDatasParametrs.SendDatas = Convert.ToBoolean(xmlTree.Element(XSendDatas_Settings).Element(XSendToURL).Value);
                }
                catch
                { 
                }

                try
                {
                    AMK_TCPIP.IP_adress = xmlTree.Element(XАМК_Settings).Element(Xip_adress).Value.ToString();
                    AMK_TCPIP.IP_port = xmlTree.Element(XАМК_Settings).Element(Xip_port).Value.ToString();
                }
                catch
                {
                }

                try
                {
                    Admissible_error.temp = xmlTree.Element(XAdmissible_error).Element(Xtemperature).Value.ToString();
                    Admissible_error.hum = xmlTree.Element(XAdmissible_error).Element(Xhumidity).Value.ToString();
                }
                catch
                {
                }
                res = true;
            }
            catch (Exception e)
            {
                if (e.InnerException != null) res = false;

                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка при чтении файла настроек (Zond_Settings.xml)" + "|"+FN_Zond_Settings+"|"+e.Message);
                writer.Close();
                MessageBox.Show("Ошибка при чтении файла настроек (Zond_Settings.xml)" + "|" + e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return res;
            }


            return res;
        }

        public void SetZondSettings()
        {
            Zond_serialPort.StopBits = ZondSerialPort.StopBits;
            Zond_serialPort.DataBits = ZondSerialPort.DataBits;
            Zond_serialPort.BaudRate = ZondSerialPort.BaudRate;
            Zond_serialPort.PortName = ZondSerialPort.PortName;
            Zond_serialPort.Parity = ZondSerialPort.Parity;
            Zond_serialPort.ReadTimeout = 100;
            
        }
        public double PowerValue(byte p)
        { 
            double res;
            if (p==18)
                    res=-30;
            else if (p==14)
                    res=-20;
            else if (p==29)
                    res=-15;
            else if (p==152)
                    res=-10;
            else if (p==96)
                    res=0;
            else if (p==132)
                    res=5;
            else if (p==200)
                    res=7;
            else if (p==192)
                    res=10;
            else
                res=-999;

            return res;
        }


        
        public bool ParsDatasBytes(byte[] buffer)
        {
            double d,d1;
            bool res=false;
            byte[] bufferCRC=new byte[30] ;
         
            

            try
            {
                if (buffer[0] == 02 && buffer[1] == 01 && buffer[33] == 10 && buffer[34] == 13)
                    {

                    for (int i = 0; i <= 29; i++)
                    {
                        bufferCRC[i] = buffer[i];
                    }

                    if (testStart == false)
                    {
                        for (int i = 2; i >= 1; i--)
                        {
                            Altitude3point[i] = Altitude3point[i - 1];
                        }
                    }

                        ZondMessage[0].Time = DateTime.Now;

                        TimeSpan delta = DateTime.Now - DTStart;

                        if (delta.TotalSeconds > 1)
                            ZondMessage[0].SecondFromStart = Convert.ToInt32(Math.Floor(delta.TotalSeconds));
                        else
                            ZondMessage[0].SecondFromStart = Convert.ToInt32(Math.Round(delta.TotalSeconds));


                        //ZondMessage[0].SecondFromStart = (DateTime.Now.Hour * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Second) - (DTStart.Hour * 60 + DTStart.Minute * 60 + DTStart.Second);


                        //ZondMessage[0].StartPriznak = buffer[1];
                        //int tmp = ZondMessage[0].StartPriznak;
                        //tmp = tmp << 8;
                        //ZondMessage[0].StartPriznak = (int)tmp;
                        //ZondMessage[0].StartPriznak = (int)(ZondMessage[0].StartPriznak | buffer[0]);

                        //  if (ZondMessage[0].StartPriznak == 258)

                        // {

                        //ZondMessage[0].ZondId = buffer[3];
                        //int tmp = ZondMessage[0].ZondId;
                        //tmp = tmp << 8;
                        //ZondMessage[0].ZondId = (int)tmp;
                        //ZondMessage[0].ZondId = (int)(ZondMessage[0].ZondId | buffer[2]);

                        int tmp;
                        ZondMessage[0].ZondId = buffer[0];
                        tmp = ZondMessage[0].ZondId;
                        tmp = tmp << 8;
                        ZondMessage[0].ZondId = (int)tmp;
                        ZondMessage[0].ZondId = (int)(ZondMessage[0].ZondId | buffer[1]);
                        ZondMessage[0].ZondId = ZondMessage[0].ZondId << 8;
                        ZondMessage[0].ZondId = (int)(ZondMessage[0].ZondId | buffer[3]);
                        ZondMessage[0].ZondId = ZondMessage[0].ZondId << 8;
                        ZondMessage[0].ZondId = (int)(ZondMessage[0].ZondId | buffer[4]);

                        ZondMessage[0].Num = buffer[5];
                        tmp = ZondMessage[0].Num;
                        tmp = tmp << 8;
                        ZondMessage[0].Num = (int)tmp;
                        ZondMessage[0].Num = (int)(ZondMessage[0].Num | buffer[4]);

                        ZondMessage[0].State = buffer[6];

                        d = (double)buffer[7];
                        ZondMessage[0].T = d - 128;
                        if (T_Demo != -999)
                        { ZondMessage[0].T = T_Demo; }


                        d = (double)buffer[8];
                        ZondMessage[0].U = Math.Round((d / 255) * 20, 2);
                        if (U_Demo != -999)
                        { ZondMessage[0].U = U_Demo; }

                        ZondMessage[0].Reserv = buffer[9];

                        //высота

                        ZondMessage[0].H_m = buffer[11];
                        tmp = ZondMessage[0].H_m;
                        tmp = tmp << 8;
                        ZondMessage[0].H_m = (int)tmp;
                        ZondMessage[0].H_m = (int)(ZondMessage[0].H_m | buffer[10]);


                        ZondMessage[0].Altitude = ZondMessage[0].H_m;
                        if (Altitude_Demo != -999)
                        { ZondMessage[0].Altitude = Altitude_Demo; }

                        if (testStart == false)
                        {
                            Altitude3point[0] = ZondMessage[0].Altitude;
                        }

                        //широта

                        ZondMessage[0].Lat = buffer[15];
                        tmp = ZondMessage[0].Lat;
                        tmp = tmp << 8;
                        ZondMessage[0].Lat = (int)tmp;
                        ZondMessage[0].Lat = (int)(ZondMessage[0].Lat | buffer[14]);
                        ZondMessage[0].Lat = ZondMessage[0].Lat << 8;
                        ZondMessage[0].Lat = (int)(ZondMessage[0].Lat | buffer[13]);
                        ZondMessage[0].Lat = ZondMessage[0].Lat << 8;
                        ZondMessage[0].Lat = (int)(ZondMessage[0].Lat | buffer[12]);
                        ZondMessage[0].Latitude = Math.Floor((double)ZondMessage[0].Lat / 1000000);
                        d = (double)ZondMessage[0].Lat / 1000000;
                        d = (d - ZondMessage[0].Latitude) * 100 / 60;
                        ZondMessage[0].Latitude = ZondMessage[0].Latitude + d;

                        if (Latitude_Demo != -999)
                        { ZondMessage[0].Latitude = Latitude_Demo; }

                        //долгота

                        ZondMessage[0].Lon = buffer[19];
                        tmp = ZondMessage[0].Lon;
                        tmp = tmp << 8;
                        ZondMessage[0].Lon = (int)tmp;
                        ZondMessage[0].Lon = (int)(ZondMessage[0].Lon | buffer[18]);
                        ZondMessage[0].Lon = ZondMessage[0].Lon << 8;
                        ZondMessage[0].Lon = (int)(ZondMessage[0].Lon | buffer[17]);
                        ZondMessage[0].Lon = ZondMessage[0].Lon << 8;
                        ZondMessage[0].Lon = (int)(ZondMessage[0].Lon | buffer[16]);
                        ZondMessage[0].Longitude = Math.Floor((double)ZondMessage[0].Lon / 1000000);
                        d = (double)ZondMessage[0].Lon / 1000000;
                        d = (d - ZondMessage[0].Longitude) * 100 / 60;
                        ZondMessage[0].Longitude = ZondMessage[0].Longitude + d;


                        if (Longitude_Demo != -999)
                        { ZondMessage[0].Longitude = Longitude_Demo; }

                        //расстояние
                        ZondMessage[0].Distance = Math.Round(distance_from_start(ZondMessage[0].Latitude, StationParametrs.Latitude, ZondMessage[0].Longitude, StationParametrs.Longitude, ZondMessage[0].Altitude, StationParametrs.Altitude), 0);
                        if (Distance_Demo != -999)
                        { ZondMessage[0].Distance = Distance_Demo; }

                        ZondMessage[0].E = Math.Round(Ugol_E(ZondMessage[0].Altitude, ZondMessage[0].Distance), 5);

                        ZondMessage[0].A = Math.Round(Azimut_A(ZondMessage[0].Latitude, StationParametrs.Latitude, ZondMessage[0].Longitude, StationParametrs.Longitude, ZondMessage[0].Altitude, StationParametrs.Altitude), 5);

                        //направление
                        ZondMessage[0].Dir = buffer[21];
                        tmp = ZondMessage[0].Dir;
                        tmp = tmp << 8;
                        ZondMessage[0].Dir = (int)tmp;
                        ZondMessage[0].Dir = (int)(ZondMessage[0].Dir | buffer[20]);
                        ZondMessage[0].Direct = (double)ZondMessage[0].Dir;
                        ZondMessage[0].Direct = Math.Round((ZondMessage[0].Direct / 65535) * 360, 2);

                        //скорость
                        ZondMessage[0].Sp = buffer[23];
                        tmp = ZondMessage[0].Sp;
                        tmp = tmp << 8;
                        ZondMessage[0].Sp = (int)tmp;
                        ZondMessage[0].Sp = (int)(ZondMessage[0].Sp | buffer[22]);
                        ZondMessage[0].Speed = (double)ZondMessage[0].Sp;
                        ZondMessage[0].Speed = Math.Round((ZondMessage[0].Speed / 65535) * 1000, 2);

                        //частота
                        if ((buffer[25] != 0) || (buffer[24] != 0))
                        {
                            //обработчик исключения для датчика влажности и температуры
                            try
                            {
                                ZondMessage[0].Fr = buffer[25];
                                tmp = ZondMessage[0].Fr;
                                tmp = tmp << 8;
                                ZondMessage[0].Fr = (int)tmp;
                                ZondMessage[0].Fr = (int)(ZondMessage[0].Fr | buffer[24]);
                                ZondMessage[0].Frequency = (double)ZondMessage[0].Fr;
                                ZondMessage[0].Frequency = Math.Round(1000000 / ZondMessage[0].Frequency, 2);

                                //Temp – температура полученная с термодатчика
                                ZondMessage[0].Temp = buffer[27];
                                tmp = ZondMessage[0].Temp;
                                tmp = tmp << 8;
                                ZondMessage[0].Temp = (int)tmp;
                                ZondMessage[0].Temp = (int)(ZondMessage[0].Temp | buffer[26]);

                                d = (double)ZondMessage[0].Temp;

                                ZondMessage[0].Temperature = Math.Round((d / ZondMessage[0].Fr - 0.1) * 200 - 100, 2);

                                if (Temperature_Demo != -999)
                                { ZondMessage[0].Temperature = Temperature_Demo; }

                                //Hum - влажность
                                ZondMessage[0].Hum = buffer[29];
                                tmp = ZondMessage[0].Hum;
                                tmp = tmp << 8;
                                ZondMessage[0].Hum = (int)tmp;
                                ZondMessage[0].Hum = (int)(ZondMessage[0].Hum | buffer[28]);

                                d = (double)ZondMessage[0].Hum;

                                ZondMessage[0].Humidity = Math.Round((d / ZondMessage[0].Fr - 0.1) * 125, 2);
                                if (Humidity_Demo != -999)
                                { ZondMessage[0].Humidity = Humidity_Demo; }

                                if (ZondMessage[0].Humidity < 0)
                                { ZondMessage[0].Humidity = (double)0; }

                            }
                            catch
                            {
                            }

                            ZondMessage[0].CRC = buffer[31];
                            tmp = ZondMessage[0].CRC;
                            tmp = tmp << 8;
                            ZondMessage[0].CRC = (int)tmp;
                            ZondMessage[0].CRC = (int)(ZondMessage[0].CRC | buffer[30]);

                            ZondMessage[0].CRC16 = CRC16_alg(bufferCRC);
                            ZondMessage[0].CRC16uint = CRC16_2(bufferCRC, 30);


                            //RSSI – уровень мощности принимаемого сигнала
                            //d = (double)buffer[32];
                            ZondMessage[0].RSSI = (double)buffer[32] - 137;

                            if (((ZondMessage[0].Humidity < (double)0) || (ZondMessage[0].Humidity > (double)100)) || ((ZondMessage[0].Temperature > (double)50) || (ZondMessage[0].Temperature < (double)-70.0)))
                            {
                                if (com_connection == true)
                                {
                                    StopАМКDatas();
                                    StopZondDatas();

                                    // MessageBox.Show(ZondMessage[0].Humidity.ToString(), "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    MessageBox.Show("К приемной станции не подключен зонд \r\nили к зонду не подключен датчик температуры и влажности! \r\n" + ZondMessage[0].Humidity, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    StopCheckThread();
                                    // stop_test_click();
                                }
                            }

                            if (((ZondMessage[0].Humidity < (double)0) || (ZondMessage[0].Humidity > (double)100)) || ((ZondMessage[0].Temperature > (double)50) || (ZondMessage[0].Temperature < (double)-70.0)))
                            {
                                res = false;
                            }
                            else
                            {
                                res = true;
                            }

                        }
                        res = true;

                    
                }
                else
                {
                    res = false;
                }
            }
            catch (Exception er)
            {
                res = false;
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка в функции ParsDatasBytes " + er.Message);
                writer.Close();

            }       
            return res;
        }

        public string Add_(string s, int poscount)       
        { 
            while (s.Length<poscount )
                s=" "+s;
            return s;
        }
        public void PrintDatasTest()
        {
            AddZondDatas_AirTemp_Temp(ZondMessage[0].Temperature.ToString("F2"));
            AddZondDatas_Hum_Relative(ZondMessage[0].Humidity.ToString("F2"));

            AddZondDatas_Altitude(ZondMessage[0].Altitude.ToString("F2"));
            AddZondDatas_Longitude(ZondMessage[0].Longitude.ToString("F6"));
            AddZondDatas_Latitude(ZondMessage[0].Latitude.ToString("F6"));
            AddZondDatas_RSSI(ZondMessage[0].RSSI.ToString("F0"));
            
           // start_check();


        }
        public void PrintOnlineDatas()
        {
            //Addlabel47text(msec.ToString());
            Addlabel46text(BitConverter.ToString(ostatokvportu));
            //Addlabel46text(ostatokvportu);
        }
        public void PrintDatas()
        {
            try
            {
                Lastmessagetime = time_now;
                string s = Lastmessagetime.ToString("dd.MM HH:mm:ss")
                    + Add_(ZondMessage[0].Num.ToString("00000"), 7)
                   + Add_(ZondMessage[0].T.ToString("F1"), 7)
                   + Add_(ZondMessage[0].U.ToString("F2"), 5)

                   + Add_(ZondMessage[0].Altitude.ToString("F2"), 10)

                   + Add_(ZondMessage[0].Longitude.ToString("F6"), 12)


                   + Add_(ZondMessage[0].Latitude.ToString("F6"), 12)



                   + Add_(ZondMessage[0].Distance.ToString("F0"), 8)

                + Add_(ZondMessage[0].Direct.ToString("F2"), 12)
                + Add_(ZondMessage[0].Speed.ToString("F2"), 8)
                + Add_(ZondMessage[0].Temperature.ToString("F2"), 9)
                + Add_(ZondMessage[0].Humidity.ToString("F2"), 8)
                + Add_(ZondMessage[0].Frequency.ToString("F1"), 8)
                + Add_(ZondMessage[0].RSSI.ToString("F0"), 8);

                string s1 = time_now.ToString("dd.MM HH:mm:ss")
                    + Add_(ZondMessage[0].Num.ToString("00000"), 7)
                   + Add_(ZondMessage[0].Altitude.ToString("F2"), 10)
                   + Add_(ZondMessage[0].Longitude.ToString("F6"), 12)
                   + Add_(ZondMessage[0].Latitude.ToString("F6"), 12)
                   + Add_(ZondMessage[0].Distance.ToString("F0"), 8)
                + Add_(ZondMessage[0].Direct.ToString("F2"), 12)
                + Add_(ZondMessage[0].Speed.ToString("F2"), 8)
                + Add_(ZondMessage[0].Temperature.ToString("F2"), 9)
                + Add_(ZondMessage[0].Humidity.ToString("F2"), 8)
                + Add_(ZondMessage[0].RSSI.ToString("F0"), 8);

                string s2 = time_now.ToString("HH:mm:ss")
                           + Add_(ZondMessage[0].Altitude.ToString("F2"), 9)
                           + Add_(ZondMessage[0].Distance.ToString("F0"), 8);

                AddStringTolistBox1(s);
                AddStringTotextBox15(s1);

                AddStringTolistBox3(s2);

                AddTemperatureGraphic(ZondMessage[0].Altitude, ZondMessage[0].Temperature);
                AddHumidGraphic(ZondMessage[0].Altitude, ZondMessage[0].Humidity);

                //AddZondDatas_AirTemp_Temp(ZondMessage[0].Temperature.ToString("F2"));
                //AddZondDatas_Hum_Relative(ZondMessage[0].Humidity.ToString("F2"));

                AddtextBox16text(ZondMessage[0].Altitude.ToString("F2"));
                AddtextBox17text(Lastmessagetime.ToString("HH:mm:ss"));
                AddtextBox19text(MessageCount.ToString());
                UpOrDown_check();

                if (checkBox1.Checked == true)
                {
                    AddZondToMap(ZondMessage[0].Latitude, ZondMessage[0].Longitude);
                }
            }
            catch (Exception er)
            {  
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в функции PrintDatas " + er.Message);
                writer.Close();
            }

        }
        public void WriteDatasToFile()
        {
            try
            {
                StreamWriter swdata = new StreamWriter(DatasFileName, true, System.Text.Encoding.GetEncoding("windows-1251"));

                string s = time_now.ToString("dd.MM HH:mm:ss") + ";"
                                + Add_(ZondMessage[0].StartPriznak.ToString("F0"), 5) + ";"
                               + Add_(ZondMessage[0].ZondId.ToString("F0"), 7) + ";"
                               + Add_(ZondMessage[0].Num.ToString("00000"), 7) + ";"
                               + Add_(ZondMessage[0].State.ToString("F0"), 7) + ";"
                              + Add_(ZondMessage[0].T.ToString("F1"), 7) + ";"
                              + Add_(ZondMessage[0].U.ToString("F2"), 5) + ";"

                              + Add_(ZondMessage[0].Altitude.ToString("F2"), 10) + ";"

                              + Add_(ZondMessage[0].Longitude.ToString("F6"), 12) + ";"


                              + Add_(ZondMessage[0].Latitude.ToString("F6"), 12) + ";"



                              + Add_(ZondMessage[0].Distance.ToString("F0"), 8) + ";"

                           + Add_(ZondMessage[0].Direct.ToString("F2"), 12) + ";"
                           + Add_(ZondMessage[0].Speed.ToString("F2"), 8) + ";"
                           + Add_(ZondMessage[0].Temperature.ToString("F2"), 9) + ";"
                           + Add_(ZondMessage[0].Humidity.ToString("F2"), 8) + ";"
                           + Add_(ZondMessage[0].Frequency.ToString("F1"), 8) + ";"
                           + Add_(ZondMessage[0].RSSI.ToString("F0"), 8) + ";";

                swdata.WriteLine(s);
                swdata.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в функции WriteDatasToFile " + er.Message);
                writer.Close();
            }
        }

        public void WriteDatasToRawFile()
        {

            StreamWriter swdata = new StreamWriter(DatasFileRaw, true, System.Text.Encoding.GetEncoding("windows-1251"));

            string s = time_now.ToString("ss")
            + Add_(ZondMessage[0].Altitude.ToString("F0"), 6) 
            + Add_(ZondMessage[0].Distance.ToString("F0"), 7) 
            + Add_(ZondMessage[0].E.ToString("F2"), 7)
            + Add_(ZondMessage[0].A.ToString("F2"), 7) 

            + Add_(ZondMessage[0].Temperature.ToString("F1"), 6)
            + Add_(ZondMessage[0].Humidity.ToString("F0"), 3)+"\n"; 

            swdata.Write(s);
            swdata.Close();
        }

        public void WriteDatasToTuFile()
        {
            try
            {
                StreamWriter swdata = new StreamWriter(DatasFileTu, true, System.Text.Encoding.GetEncoding("windows-1251"));

                string s = ZondMessage[0].SecondFromStart.ToString() + Convert.ToChar(0x09)
                           + ZondMessage[0].Temperature.ToString() + Convert.ToChar(0x09)
                           + ZondMessage[0].Humidity.ToString();

                swdata.WriteLine(s);

                swdata.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в функции WriteDatasToTuFile " + er.Message);
                writer.Close();
            }
        }

        public void WriteDatasToCrdFile()
        {
            try
            {
                StreamWriter swdata = new StreamWriter(DatasFileCrd, true, System.Text.Encoding.GetEncoding("windows-1251"));

                string s = ZondMessage[0].SecondFromStart.ToString() + Convert.ToChar(0x09)
                           + ZondMessage[0].Distance.ToString() + Convert.ToChar(0x09)
                           + ZondMessage[0].A.ToString() + Convert.ToChar(0x09)
                           + ZondMessage[0].E.ToString();

                swdata.WriteLine(s);

                swdata.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в функции WriteDatasToCrdFile " + er.Message);
                writer.Close();
            }
        }

        public void WriteDatasToHexFile(string str)
        {
            try
            {
                //string str_time00 = DateTime.Now.ToString("yyyy.dd.MM HH:mm:00");              
                //DateTime time00 = DateTime.ParseExact(str_time00, "yyyy.dd.MM HH:mm:ss", null);
                //TimeSpan delta = DateTime.Now - time00;                         
                       
                StreamWriter swdata = new StreamWriter(DatasFileHexName, true, System.Text.Encoding.GetEncoding("windows-1251"));
                swdata.WriteLine(time_now.ToString("dd.MM HH:mm:ss") + "  " + str);
                swdata.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в файл hex " +er.Message);
                writer.Close();
            }
        }

        public double distance_from_start(double lat1, double lat2, double lon1, double lon2, double Alt1, double Alt2)
        {
            double res = -999;
            double L,dL;
            double R;
            double dfi;

            try
            {
                dfi = Math.Acos(Math.Sin(lat1 * Math.PI / 180) * Math.Sin(lat2 * Math.PI / 180) + Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) * Math.Cos(Math.Abs(lon2 * Math.PI / 180 - lon1 * Math.PI / 180)));
                R = Math.Cos(Math.Abs(lat2 * Math.PI / 180 - lat1 * Math.PI / 180)) * 21340 + 6356860;
                L = dfi * R;
                dL=Math.Sqrt(Math.Pow((Alt2-Alt1),2)+Math.Pow(L,2));
                res = dL;
            }
            catch
            {
             
            }

            return res;
        }
        public double Ugol_E(double H, double D)
        {
            double res = -999;

            double L, dL;
            double R;
            double E;
            double dH;
            try
            {
                dH = H - StationParametrs.Altitude;
                E = Math.Asin(dH / D);
                res = E;
            }
            catch
            {

            }
            return res;
        }

        public double Azimut_A(double lat1, double lat2, double lon1, double lon2, double Alt1, double Alt2)
        {
            double res = -999;
            double Rz;

            double L;
            double dfi;
            try
            {
              
                double dlon = lon1 - lon2;

               dfi = Math.Acos(Math.Sin(lat1 * Math.PI / 180) * Math.Sin(lat2 * Math.PI / 180) + Math.Cos(lat1 * Math.PI / 180) * Math.Cos(lat2 * Math.PI / 180) * Math.Cos(Math.Abs(lon2 * Math.PI / 180 - lon1 * Math.PI / 180)));
               Rz = Math.Cos(Math.Abs(lat2 * Math.PI / 180 - lat1 * Math.PI / 180)) * 21340 + 6356860;
               L = dfi * Rz;
               double dist = L;
               
               double dlat = lat1 - lat2;
               dlat = dlat * (2 * Math.PI * Rz / 360);

               double beta = Math.Asin(Math.Abs(dlat) / dist) *(180/ Math.PI);

               double angle=-999;
                if (dlon < 0) 
                {
	                if (dlat > 0) 
                    {
    	                angle = 270 + beta;
                    }
                        else
                    {
    	                angle = 270 - beta;
                    }
                }
                else
                {
	                if (dlat > 0) 
                    {
    	                angle = 90 - beta;
                    }
                    else
                    {
    	                angle = 90 + beta;
                    }
                }

                res = angle;
            }
            catch
            {

            }

            return res;
        }


        public bool ReadMessagefromZond()
        {
            string s;
            bool res = false;
           // byte[] buffer = new byte[30];
           byte[] buffer;


            s="";
            try
            {

                buffer = new byte[Zond_serialPort.BytesToRead];
                Zond_serialPort.Read(buffer, 0, buffer.Length);
                
                 string s1=buffer.Length.ToString();
                 
                 if (buffer.Length == 35)
                 {
                     buffer_start = new byte[35];
                     for (int i = 0; i < 35; i++)
                     {
                         buffer_start[i] = buffer[i];
                     }
                     buffer = new byte[35];

                     buffer = buffer_start;
                 }

                 if (buffer.Length == 35)
                 {
                     if (ParsDatasBytes(buffer) == true)
                     {
                         s = "";
                         s = BitConverter.ToString(buffer);
                         WriteDatasToHexFile(s);
                         listBox2.Items.Add(DateTime.Now.ToString("dd.MM HH:mm:ss") + " " + s);
                         listBox2.SelectedIndex = listBox2.Items.Count - 1;

                         PrintDatas();
                         WriteDatasToFile();
                         WriteDatasToRawFile();
                         if (SendDatasParametrs.SendDatas)
                             sendToserver();
                     }
                 }
                 else if (buffer.Length != 0 && buffer.Length < 35)
                 {
                     if (priznak_part_of_send == 1)
                     {
                         buffer_start = new byte[buffer.Length];
                         buffer_start = buffer;
                         priznak_part_of_send = 2;
                     }
                     else if (priznak_part_of_send == 2)
                     {
                         buffer_end = new byte[buffer.Length];
                         buffer_end = buffer;
                         buffer = new byte[buffer_start.Length + buffer_end.Length];
                         buffer = buffer_start.Concat(buffer_end).ToArray();
                         priznak_part_of_send = 1;
                         if (buffer.Length == 35)
                         {
                             if (ParsDatasBytes(buffer) == true)
                             {
                                 s = "";
                                 s = BitConverter.ToString(buffer);
                                 WriteDatasToHexFile(s);
                                 listBox2.Items.Add(DateTime.Now.ToString("dd.MM HH:mm:ss") + " " + s);
                                 listBox2.SelectedIndex = listBox2.Items.Count - 1;

                                 PrintDatas();
                                 WriteDatasToFile();
                                 WriteDatasToRawFile();
                                 if (SendDatasParametrs.SendDatas)
                                     sendToserver();
                             }
                         }
                     }
                 }                                        
                
            }
            catch (Exception e)
            {
                if (e.InnerException != null) res = false;

                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : " + e.Message);
                writer.Close();
            }


            return res;
        }
        public bool ReadMessagefromZondTextFile()
        {

            bool res = false;

            res = true;
            return res;
        }
        void CreateInfoFile(string filename, DateTime DTn)
        {

         if (ReadInfoDatas()==true)
        {

            
            DatasFileInfo = filename;

            if (ReadZondSettigs() == true)
            {
                StreamWriter swdata = new StreamWriter(DatasFileInfo, false, System.Text.Encoding.GetEncoding("windows-1251"));
                swdata.WriteLine("StationSynopticIndex:" + Convert.ToChar(0x09) + StationParametrs.SynopIndex.ToString());
                swdata.WriteLine("StationLongitude:" + Convert.ToChar(0x09) + StationParametrs.Longitude.ToString());
                swdata.WriteLine("StationLattitude:" + Convert.ToChar(0x09) + StationParametrs.Latitude.ToString());
                swdata.WriteLine("StationHeightAboveSeaLevel:" + Convert.ToChar(0x09) + StationParametrs.Altitude.ToString());

                swdata.WriteLine("OnGroundPressure:" + Convert.ToChar(0x09) + InfoDatas.OnGroundPressure);
                swdata.WriteLine("OnGroundWindDirection:" + Convert.ToChar(0x09) + InfoDatas.OnGroundWindDirection);
                swdata.WriteLine("OnGroundWindVelocity:" + Convert.ToChar(0x09) + InfoDatas.OnGroundWindVelocity);
                swdata.WriteLine("OnGroundHumidityError:" + Convert.ToChar(0x09) + InfoDatas.OnGroundHumidityError);
                swdata.WriteLine("OnGroundTemperatureError:" + Convert.ToChar(0x09) + InfoDatas.OnGroundTemperatureError);

                swdata.WriteLine("StartYear:" + Convert.ToChar(0x09) + DTn.ToString("yyyy"));
                swdata.WriteLine("StartMonth:" + Convert.ToChar(0x09) + DTn.ToString("MM"));
                swdata.WriteLine("StartDay:" + Convert.ToChar(0x09) + DTn.ToString("dd"));
                swdata.WriteLine("StartHour:" + Convert.ToChar(0x09) + DTn.ToString("HH"));
                swdata.WriteLine("StartMinute:" + Convert.ToChar(0x09) + DTn.ToString("mm"));

                swdata.WriteLine("NebulosityCode:" + Convert.ToChar(0x09) + InfoDatas.NebulosityCode);
                swdata.WriteLine("RadioZondType:" + Convert.ToChar(0x09) + InfoDatas.ZondType);

                swdata.Close();

            }

        }


        }

        private void button1_Click(object sender, EventArgs e)
        {

            DialogResult result = DialogResult.Yes;
            if (label29.Text!="Готов")
            {
                result = MessageBox.Show("Зонд к выпуску не готов. Продолжить?", "Зонд не готов!", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                {
                    if (result == DialogResult.No) //Если нажал нет
                    {
                        return;
                    }
                }
            }



            Lastmessagetime = System.Convert.ToDateTime("01.01.2000");
            testStart = false;

            StopАМКDatas();
            StopZondDatas();


            textBox16.Text = "";
            textBox17.Text = "";
            textBox18.Text = "";
            textBox19.Text = "";
            label41.Text = "";

            groupBox2.Visible = true;
            groupBox6.Visible = true;

            button5.Enabled = false;
            button4.Enabled = false;

            DateTime DTNow=DateTime.Now;
            DTStart = DTNow;

            string s = System.AppDomain.CurrentDomain.BaseDirectory + "Result Files\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm");

            ResultFolder = s;


            if (Directory.Exists(s) == false)
            {
                Directory.CreateDirectory(s);
            }

            //копрование файла info
            label45.Visible = false;
            if ((File.Exists(System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml")))
            {
                label45.ForeColor = Color.DarkRed;


                FileInfo fi = new FileInfo(System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml");
                if (fi.LastWriteTime.Day != DTNow.Day)
                {
                    label45.Visible = true;
                    label45.Text = "Данные приземных метеопараметров сохранены " + fi.LastWriteTime.ToShortDateString() + " !";
                }
                
                else
                {
                    int dt = (DTNow.Hour * 60 + DTNow.Minute ) - (fi.LastWriteTime.Hour * 60 + fi.LastWriteTime.Minute);
                    if (dt > 30)
                    {
                        label45.Visible = true;
                        label45.Text = "Данные приземных метеопараметров сохранены " + fi.LastWriteTime.ToString("HH:mm") + " !";
                    }
                }
                CreateInfoFile(ResultFolder + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".info",DTNow);

               // File.Copy(System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml", ResultFolder + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".info", true);
            }
            else
            {
                label45.ForeColor = Color.Red;
                label45.Visible = true;
                label45.Text = "Данные приземных метеопараметров не сохранены !";
            }
            //

            DatasFileName = s + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".csv";


            StreamWriter swdata = new StreamWriter(DatasFileName, true, System.Text.Encoding.GetEncoding("windows-1251"));
            swdata.WriteLine("Время;Старт;Idзонда;№;Состояние;T;U;Высота, м;Долгота, грд;Широта, грд;Дальность, м;Напр., грд;Скор., м/с;Темп.,C;Влажн., %;частота, Гц;Сигнал, дБ;");
            swdata.Close();

            DatasFileHexName = s + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".hex";



            listBox1.Items.Clear();
            listBox2.Items.Clear();
            listBox3.Items.Clear();
            chart1.Series[0].Points.Clear();
            chart2.Series[0].Points.Clear();

            gmap.Overlays.Clear();
            gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new GMap.NET.PointLatLng(StationParametrs.Latitude, StationParametrs.Longitude);


            ZondMessage = new ZondMessageType[1];
            Altitude3point = new double[3];
            Altitude3point[0] = 0; Altitude3point[1] = 0; Altitude3point[2] = 0;


            if (ReadZondSettigs() == true)
            {
                ReadZondDemo();

                //DatasFileRaw = s + "Result Files\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".RAW";
                //swdata = new StreamWriter(DatasFileRaw, true, System.Text.Encoding.GetEncoding("windows-1251"));


                //swdata.Write("ИСХОДНЫЕ ДАННЫЕ ЗОНДИРОВАНИЯ ЗА " + DTNow.ToString("dd.MM.yyyy HH:mm")+"\n");
                //swdata.Write("ПРИЗЕМНЫЙ ВЕТЕР (D V) :"+""+""+ "\n");
                //swdata.Write("ПРИЗЕМНОЕ ДАВЛЕНИЕ :"+ ""+ "\n");
                //swdata.Write("РАСПОЛОЖЕНИЕ СТАНЦИИ : " + "\n" + "    ШИРОТА :" + Add_(StationParametrs.Latitude.ToString("F0"), 4) + " ДОЛГОТА :" + Add_(StationParametrs.Longitude.ToString("F0"), 4) + "\n" + "    ВЫСОТА НАД УРОВНЕМ МОРЯ :" + Add_(StationParametrs.Altitude.ToString("F0"), 6) + "\n");
                //swdata.Write("СИНОПТИЧЕСКИЙ ИНДЕКС СТАНЦИИ :" + Add_(StationParametrs.SynopIndex.ToString("F0"), 6) + "\n");
                //swdata.Write("КОД ОБЛАЧНОСТИ :" +" " + "\n");

                //swdata.Write("ВРЕМ     H      D      E      A     T  U" + "\n");
                //swdata.Close();

                DatasFileTu = s + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".tu";
                swdata = new StreamWriter(DatasFileTu, true, System.Text.Encoding.GetEncoding("windows-1251"));
                swdata.Close();

                DatasFileCrd = s + "\\" + DTNow.ToString("yyyyMMdd") + "-" + DTNow.ToString("HHmm") + ".crd";
                swdata = new StreamWriter(DatasFileCrd, true, System.Text.Encoding.GetEncoding("windows-1251"));
                swdata.Close();


                SetZondSettings();

                try
                {


                    if (Zond_serialPort.IsOpen == true)
                        Zond_serialPort.Close();


                    //Zond_serialPort.Open();
                    //timer1.Interval = ZondSerialPort.ZondPeriodSend;
                    //priznak_part_of_send = 1;
                    //timer1.Start();
                    //---------
                    t = new Thread(StartListen);
                    t.IsBackground = true;
                    t.Start();
                    //--------
                    button1.Enabled = false;
                    button2.Enabled = true;
                }
                catch (Exception er)
                {
                    StreamWriter writer = null;
                    writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                    writer.WriteLine(DTNow.ToString() + " : Ошибка подключения " + er.Message);
                    writer.Close();
                    MessageBox.Show("Ошибка подключения: " + er.Message, "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }

            LastMessageTimeCheckThreadStart();
        }
        public void StartListeTest()
        {

            try
            {
                Zond_serialPort.Open();

                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Подключение к порту успешно");
                writer.Close();
                ZondLabel_Status("Подключение к приемной станции зонда успешно установлено");
                com_connection = true;
            }
            catch (Exception er)
            {
                com_connection = false;
                ZondLabel_Status("Подключение к приемной станции зонда НЕ УСТАНОВЛЕНО");

                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка подключения " + er.Message);
                writer.Close();
                MessageBox.Show("Ошибка подключения: " + er.Message, "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            //============================================================================
            //Вариант 2
            //Данные пакета выводятся сразу после получения;
            //Конец текущего пакета определяется, если разница между временем получения
            //текущего байта и предыдущего байтов более 0,5 сек.
            byte[] buffer = new byte[35];

            counter = 0;
            while (true)
            {
                if ((Zond_serialPort.IsOpen) & (t_zond.IsAlive))
                {
                    try
                    {
                        a_byte = Zond_serialPort.ReadByte();
                        buffer[counter] = System.Convert.ToByte(a_byte);
                     
                        counter++;
                        dt1 = DateTime.Now;
                        if (counter == 1)
                        {
                            dt = dt1;
                        }
                        msec = dt1.Subtract(dt).Ticks / 10000;

                        if ((counter == 35) || (msec > ZondSerialPort.ZondPeriodSend))
                        {
                            if (counter == 35)
                            {
                                if (ParsDatasBytes(buffer) == true)
                                {

                                    PrintDatasTest();
                                    ZondLabel_Status("Поступают");

                                }
                            }
                            counter = 0;
                            buffer = new byte[35];
                           
                        }


                    }
                    catch (Exception er)
                    {
                        if (msec > ZondSerialPort.ZondPeriodSend)
                        {
                            ZondLabel_Status("Данные не посутают");
                        }
                        
                        //StreamWriter writer = null;
                        //writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                        //writer.WriteLine(DateTime.Now.ToString() + " : Error reading data " + er.Message);
                        //writer.Close();

                    }
                }
                else if (Zond_serialPort.IsOpen==false)
                {
                    ZondLabel_Status("Подключение к приемной станции зонда НЕ УСТАНОВЛЕНО");
                }
            }
            //============================================================================
        
        }

        public void StartListen()
        {
            try
            {                
                Zond_serialPort.Open();

                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Подключение к порту успешно" );
                writer.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                    writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                    writer.WriteLine(DateTime.Now.ToString() + " : Ошибка подключения "+er.Message);
                    writer.Close();
                    MessageBox.Show("Ошибка подключения: "+er.Message,"Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Warning );
                return;
            }       

            //============================================================================
            //Вариант 2
            //Данные пакета выводятся сразу после получения;
            //Конец текущего пакета определяется, если разница между временем получения
            //текущего байта и предыдущего байтов более 0,5 сек.
            byte[] buffer = new byte[35];

            counter = 0;
            MessageCount = 0;
            while (true)
            {
                if ((Zond_serialPort.IsOpen) & (t.IsAlive))
                {
                    try
                    {
                        a_byte = Zond_serialPort.ReadByte();
                        buffer[counter] = System.Convert.ToByte(a_byte);
                       // bufferOnline[counter] = buffer[counter];
                        counter++;
                        dt1 = DateTime.Now;
                        
                        if (counter == 1)
                        {
                            dt = dt1;
                            time_now = DateTime.Now;

                        }                        
                        msec = dt1.Subtract(dt).Ticks / 10000;
                        //
                        if ((counter == 35) || (msec > ZondSerialPort.ZondPeriodSend))
                        {
                            if (counter == 35)
                            {                                
                                if (ParsDatasBytes(buffer) == true)
                                {
                                    s = "";
                                    s = BitConverter.ToString(buffer);
                                    WriteDatasToHexFile(s);

                                    AddStringTolistBox2(time_now.ToString("dd.MM HH:mm:ss") + " " + s);

                                    MessageCount++;

                                    PrintDatas();

                                    WriteDatasToFile();
                                    WriteDatasToTuFile();
                                    WriteDatasToCrdFile();
                                    // WriteDatasToRawFile();
                                    

                                    if (SendDatasParametrs.SendDatas)
                                        sendToserver();
                                } 
                            }
                            counter = 0;
                            buffer = new byte[35];
                            bufferOnline=new byte[35];
                            
                            
                            ostatokvportu = new byte[Zond_serialPort.BytesToRead];
                            Zond_serialPort.Read(ostatokvportu, 0, ostatokvportu.Length);

                            //PrintOnlineDatas();
                        }


                    }
                    catch (Exception er)
                    
                    {
                        
                        //StreamWriter writer = null;
                        //writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                        //writer.WriteLine(DateTime.Now.ToString() + " : Error reading data " + er.Message);
                        //writer.Close();

                    }
                }
            }
            //============================================================================

        }

        private delegate void SetTextDelegate(string s);

        private delegate void SetDoubleLonLatDelegate(double Lat, double Lon);


        private void AddStringTolistBox2(string s)
        {
            if (listBox2.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddStringTolistBox2);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                listBox2.Items.Add(s);
                listBox2.SelectedIndex = listBox2.Items.Count - 1;
            }
        }


        private void AddStringTotextBox15(string s)
        {
            if (textBox15.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddStringTotextBox15);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox15.Text=(s);
            }
        }

        private void AddStringTolistBox1(string s)
        {
            if (listBox1.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddStringTolistBox1);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                listBox1.Items.Add(s);
                listBox1.SelectedIndex = listBox1.Items.Count - 1;
            }
        }

        private void AddStringTolistBox3(string s)
        {
            if (listBox3.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddStringTolistBox3);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                listBox3.Items.Add(s);
                listBox3.SelectedIndex = listBox1.Items.Count - 1;
            }
        }

         
        private void AddStationToMap(double Lat,double Lon)
        {
            if (gmap.InvokeRequired)
            {
                SetDoubleLonLatDelegate SetDoubleLonLatDelegate = new SetDoubleLonLatDelegate(AddZondToMap);
                this.Invoke(SetDoubleLonLatDelegate, new object[] { Lat,Lon });
            }
            else
            {
                markersOverlay = new GMapOverlay("markers");

                Points = new PointsType[1];
                Points[0].Lat = Lat;
                Points[0].Lon = Lon;
                Points[0].ID = "";
                Points[0].Name = "Станция";

                Points[0].marker = new GMarkerGoogle(new PointLatLng(Points[0].Lat, Points[0].Lon),new Bitmap("Station.png"));
                Points[0].marker.ToolTipText = Points[0].Name;
                markersOverlay.Markers.Add(Points[0].marker);

                gmap.Overlays.Add(markersOverlay);

            }

        }

        private void AddZondToMap(double Lat,double Lon)
        {
            try
            {
                if (gmap.InvokeRequired)
                {
                    SetDoubleLonLatDelegate SetDoubleLonLatDelegate = new SetDoubleLonLatDelegate(AddZondToMap);
                    this.Invoke(SetDoubleLonLatDelegate, new object[] { Lat, Lon });
                }
                else
                {
                    markersOverlay = new GMapOverlay("markers");

                    Points = new PointsType[1];
                    Points[0].Lat = Lat;
                    Points[0].Lon = Lon;
                    Points[0].ID = "";
                    Points[0].Name = "Зонд";

                    Points[0].marker = new GMarkerGoogle(new PointLatLng(Points[0].Lat, Points[0].Lon), new Bitmap("zonde.png"));
                    Points[0].marker.ToolTipText = Points[0].Name;
                    markersOverlay.Markers.Add(Points[0].marker);




                    gmap.Overlays.Add(markersOverlay);

                    gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
                    GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
                    gmap.Position = new GMap.NET.PointLatLng(ZondMessage[0].Latitude, ZondMessage[0].Longitude);
                }
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка записи в функции AddZondToMap " + er.Message);
                writer.Close();
            }



        }

        private delegate void SetDoubleAltTempDelegate(double Alt, double Temp);

        private void AddTemperatureGraphic(double Alt, double Temp)
        {
            if (chart1.InvokeRequired)
            {
                SetDoubleAltTempDelegate SetDoubleAltTempDelegate = new SetDoubleAltTempDelegate(AddTemperatureGraphic);
                this.Invoke(SetDoubleAltTempDelegate, new object[] { Alt, Temp });
            }
            else
            {
                chart1.Series[0].Points.AddXY(Temp, Alt);
            }

        }

        private delegate void SetDoubleAltHumDelegate(double Alt, double Hum);
        private void AddHumidGraphic(double Alt, double Hum)
        {
            if (chart2.InvokeRequired)
            {
                SetDoubleAltHumDelegate SetDoubleAltHumDelegate = new SetDoubleAltHumDelegate(AddHumidGraphic);
                this.Invoke(SetDoubleAltHumDelegate, new object[] { Alt, Hum });
            }
            else
            {
                chart2.Series[0].Points.AddXY(Hum, Alt);
            }

        }

           

        private void timer1_Tick(object sender, EventArgs e)
        {
            ReadMessagefromZond(); 
        }

        private void button2_Click(object sender, EventArgs e)
        {
            testStart = false;
            LastMessageTimeCheckThreadStop();
            StopАМКDatas();
           
            //timer1.Stop();
            //Zond_serialPort.Close();
            //button1.Enabled = true;
            //button2.Enabled = false;

            try
            {
                Zond_serialPort.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка закрытия порта " + er.Message);
                writer.Close();
                MessageBox.Show("Ошибка закрытия порта: " + er.Message, "Ошибка закрытия порта", MessageBoxButtons.OK, MessageBoxIcon.Warning);
             
            }
            if (t != null)
            {
                t.Abort();
                t = null;
            }
 
            button1.Enabled = true;
            button2.Enabled = false;

            button5.Enabled = true;
            button4.Enabled = false;

            label41.Text="Выпуск завершен";
            label41.ForeColor = Color.DarkBlue;

        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            bool bl = f2.ShowFormAbout();
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (ReadZondSettigs() == true)
            {
                string FN_Zond_Settings = System.AppDomain.CurrentDomain.BaseDirectory + "Zond_Settings.xml";
                if (ZondMessage != null )
                {
                    bool bl = f3.ShowFormSettings(StationParametrs, ZondSerialPort, SendDatasParametrs,AMK_TCPIP,Admissible_error, FN_Zond_Settings, ZondMessage[0].Longitude.ToString("F6"), ZondMessage[0].Latitude.ToString("F6"));
                }
                else
                {
                    bool bl = f3.ShowFormSettings(StationParametrs, ZondSerialPort, SendDatasParametrs,AMK_TCPIP,Admissible_error, FN_Zond_Settings, "", "");
                }

            }
            Admissible_error_print();

       }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (button1.Enabled==true)
                listBox2.SelectedIndex = listBox1.SelectedIndex; 
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (button1.Enabled == true)
                listBox1.SelectedIndex = listBox2.SelectedIndex;
        }


        private void Form1_Resize(object sender, EventArgs e)
        {
       

           // groupBox1.Height = 240+((groupBox2.Height - 514)/2);
           // listBox1.Height = groupBox1.Height - 40;
           // groupBox3.Top = groupBox1.Top + groupBox1.Height;
           // groupBox3.Height = 224 + ((groupBox2.Height - 514) / 2)-10;

           // tabControl1.Top = groupBox2.Top;
           // tabControl1.Height = 224 + ((groupBox2.Height - 514) / 2) - 10;
        }


        public void sendToserver()
        {

            string sendReqwest="/set/?start="+ZondMessage[0].StartPriznak.ToString("F0")

                   +"&id="+ZondMessage[0].ZondId.ToString("F0")
                   +"&num="+ZondMessage[0].Num.ToString("00000")
                   +"&cond="+ZondMessage[0].State.ToString("F0")
                   +"&t_in="+ZondMessage[0].T.ToString("F1")
                   +"&v="+ZondMessage[0].U.ToString("F2")
                   +"&h="+ZondMessage[0].Altitude.ToString("F2")
                   +"&x="+ZondMessage[0].Longitude.ToString("F6")
                   +"&y="+ZondMessage[0].Latitude.ToString("F6")
                   +"&alpha="+ZondMessage[0].Direct.ToString("F2")
                   +"&speed="+ZondMessage[0].Speed.ToString("F2")
                   +"&tay="+ZondMessage[0].Frequency.ToString("F1")
                   +"&t="+ZondMessage[0].Temperature.ToString("F2")
                   +"&rh="+ZondMessage[0].Humidity.ToString("F2")
                   +"&crc=" + ZondMessage[0].CRC.ToString();

         //   string sendReqwest="/set/?start=20&id=666




            HttpGET(SendDatasParametrs.URL, sendReqwest);
       
        }
        public string HttpGET(string Url, string Data)
        {
            string Out="false";
            try
            {
                System.Net.WebRequest req = System.Net.WebRequest.Create(Url + "?" + Data);
                System.Net.WebResponse resp = req.GetResponse();
                System.IO.Stream stream = resp.GetResponseStream();
                System.IO.StreamReader sr = new System.IO.StreamReader(stream);
                Out = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception er)
            {
                StreamWriter writer = null;
                writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                writer.WriteLine(DateTime.Now.ToString() + " : Ошибка отправки на интернет сервер " + er.Message);
                writer.Close();
             //   MessageBox.Show("Ошибка отправки на интернет сервер: " + er.Message, "Ошибка отправки на интернет сервер", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            return Out;

        }
        public void PrintZondWay(double lon,double lat)
        
        {
            markersOverlay = new GMapOverlay("markers");

            Points = new PointsType[1];
            Points[0].Lat = lat;
            Points[0].Lon=lon;
            Points[0].ID = "";
            Points[0].Name="Зонд";

            Points[0].marker = new GMarkerGoogle(new PointLatLng(Points[0].Lat, Points[0].Lon), GMarkerGoogleType.blue);
            Points[0].marker.ToolTipText = Points[0].Name;
            markersOverlay.Markers.Add(Points[0].marker);
            
             
            gmap.Overlays.Add(markersOverlay);

            gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
            GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
            gmap.Position = new GMap.NET.PointLatLng(ZondMessage[0].Latitude, ZondMessage[0].Longitude);

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {


        }

        private void gmap_Load(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            Admissible_error_print();
            FullStartSettingsPage();
        }

        private void Admissible_error_print()
        {
            if (ReadZondSettigs() == true)
            {
                label37.Text = Admissible_error.temp;
                label38.Text = Admissible_error.hum;

                label37.Visible = true;
                label38.Visible = true;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                if (ReadZondSettigs() == true)
                {
                    try
                    {
                        gmap.Overlays.Clear();

                        markersOverlay = new GMapOverlay("markers");

                        

                        Points = new PointsType[1];
                        Points[0].Lat = StationParametrs.Latitude;
                        Points[0].Lon = StationParametrs.Longitude;
                        Points[0].ID = "";
                        Points[0].Name = "Станция";

                        Points[0].marker = new GMarkerGoogle(new PointLatLng(Points[0].Lat, Points[0].Lon), new Bitmap("Station.png"));
                        Points[0].marker.ToolTipText = Points[0].Name;
                        markersOverlay.Markers.Add(Points[0].marker);

                        gmap.Overlays.Add(markersOverlay);


                        gmap.MapProvider = GMap.NET.MapProviders.OpenStreetMapProvider.Instance;
                        GMap.NET.GMaps.Instance.Mode = GMap.NET.AccessMode.ServerAndCache;
                        gmap.Position = new GMap.NET.PointLatLng(StationParametrs.Latitude, StationParametrs.Longitude);
                        
                        
                    
                    }
                    catch (Exception er)
                    {

                    }
                }
            }

        }

        private void данныеЗондированияToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (Directory.Exists(ResultFolder) == false)
            {
                ResultFolder = System.AppDomain.CurrentDomain.BaseDirectory + "Result Files\\";
            }


            System.Diagnostics.Process.Start("explorer", ResultFolder);

        }


        public bool ReadInfoDatas()
        {
            bool res = false;
            int i;
            string FN_InfoDatas = System.AppDomain.CurrentDomain.BaseDirectory;
            FN_LogZond = System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.log";

            FN_InfoDatas = System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml";

            string XMLInfoDatas = System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml";

            try
            {

                StreamReader XMLStream = new StreamReader(FN_InfoDatas);

                XMLInfoDatas = XMLStream.ReadToEnd();

                XMLStream.Close();

                XElement xmlTree = XElement.Parse(XMLInfoDatas);

                XName XInfoDatas = "InfoDatas";
                XName XOnGroundPressure = "OnGroundPressure";
                XName XOnGroundWindDirection = "OnGroundWindDirection";

                XName XOnGroundWindVelocity = "OnGroundWindVelocity";
                XName XOnGroundHumidityError = "OnGroundHumidityError";
                XName XOnGroundTemperatureError = "OnGroundTemperatureError";
                XName XNebulosityCode = "NebulosityCode";
                XName XRadioZondType = "RadioZondType";

                try
                {

                    string ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

                    try
                    {
                        InfoDatas.OnGroundPressure = xmlTree.Element(XOnGroundPressure).Value;
                        if (ds == ".")
                            InfoDatas.OnGroundPressure = InfoDatas.OnGroundPressure.Replace(",", ".");
                        else if (ds == ",")
                            InfoDatas.OnGroundPressure = InfoDatas.OnGroundPressure.Replace(".", ",");

                        InfoDatas.OnGroundWindDirection = xmlTree.Element(XOnGroundWindDirection).Value;
                        if (ds == ".")
                            InfoDatas.OnGroundWindDirection = InfoDatas.OnGroundWindDirection.Replace(",", ".");
                        else if (ds == ",")
                            InfoDatas.OnGroundWindDirection = InfoDatas.OnGroundWindDirection.Replace(".", ",");

                        InfoDatas.OnGroundWindVelocity = xmlTree.Element(XOnGroundWindVelocity).Value;
                        if (ds == ".")
                            InfoDatas.OnGroundWindVelocity = InfoDatas.OnGroundWindVelocity.Replace(",", ".");
                        else if (ds == ",")
                            InfoDatas.OnGroundWindVelocity = InfoDatas.OnGroundWindVelocity.Replace(".", ",");

                        InfoDatas.OnGroundHumidityError = xmlTree.Element(XOnGroundHumidityError).Value;
                        if (ds == ".")
                            InfoDatas.OnGroundHumidityError = InfoDatas.OnGroundHumidityError.Replace(",", ".");
                        else if (ds == ",")
                            InfoDatas.OnGroundHumidityError = InfoDatas.OnGroundHumidityError.Replace(".", ",");

                        InfoDatas.OnGroundTemperatureError = xmlTree.Element(XOnGroundTemperatureError).Value;
                        if (ds == ".")
                            InfoDatas.OnGroundTemperatureError = InfoDatas.OnGroundTemperatureError.Replace(",", ".");
                        else if (ds == ",")
                            InfoDatas.OnGroundTemperatureError = InfoDatas.OnGroundTemperatureError.Replace(".", ",");

                        InfoDatas.NebulosityCode = xmlTree.Element(XNebulosityCode).Value;
                        InfoDatas.ZondType = xmlTree.Element(XRadioZondType).Value;
                    }
                    catch
                    {
                    }

                    res = true;

                }
                catch (Exception e)
                {
                    if (e.InnerException != null) res = false;

                    StreamWriter writer = null;
                    writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                    writer.WriteLine(DateTime.Now.ToString() + " : Ошибка при чтении файла InfoDatas.xml" + "|" + FN_InfoDatas + "|" + e.Message);
                    writer.Close();
                    MessageBox.Show("Ошибка при чтении файла настроек InfoDatas.xml" + "|" + e.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return res;
                }

            }
            catch 
            {
            }
            return res;
        }


        public void FullStartSettingsPage()
        {

            InfoDatas=new InfoDatasType();
            ReadInfoDatas();

            maskedTextBox2.Text = DateTime.Now.ToString("HH") + ':' + DateTime.Now.ToString("mm");
            dateTimePicker2.Value = DateTime.Now;

          //  textBox3.Text = InfoDatas.OnGroundPressure;
         //   textBox1.Text= InfoDatas.OnGroundWindDirection;
         //   textBox2.Text= InfoDatas.OnGroundWindVelocity;
         //   textBox6.Text= InfoDatas.OnGroundHumidityError;
        //    textBox5.Text= InfoDatas.OnGroundTemperatureError;
        //    textBox4.Text= InfoDatas.NebulosityCode;
            maskedTextBox1.Text = InfoDatas.ZondType;
        }


        private void tabControl1_Click(object sender, EventArgs e)
        {


        }

        private void button3_Click(object sender, EventArgs e)
        {


            string FN_InfoDatas = System.AppDomain.CurrentDomain.BaseDirectory + "InfoDatas.xml";
            try
            {

                XElement xmlSettingsDoc = new XElement("InfoDatas",
                    new XElement("OnGroundPressure"), new XElement("OnGroundWindDirection"), new XElement("OnGroundWindVelocity"), new XElement("OnGroundHumidityError"), new XElement("OnGroundTemperatureError"), new XElement("NebulosityCode"), new XElement("RadioZondType"));
                xmlSettingsDoc.Element("OnGroundPressure").Value = textBox3.Text;
                xmlSettingsDoc.Element("OnGroundWindDirection").Value = textBox1.Text;
                xmlSettingsDoc.Element("OnGroundWindVelocity").Value = textBox2.Text;
                xmlSettingsDoc.Element("OnGroundHumidityError").Value = textBox6.Text;
                xmlSettingsDoc.Element("OnGroundTemperatureError").Value = textBox5.Text;
                xmlSettingsDoc.Element("NebulosityCode").Value = textBox4.Text;
                xmlSettingsDoc.Element("RadioZondType").Value = maskedTextBox1.Text;

                StreamWriter xmlSettingsWr = new StreamWriter(FN_InfoDatas);
                xmlSettingsWr.Write(xmlSettingsDoc);
                xmlSettingsWr.Close();

                MessageBox.Show("Инофрмация о приземных параметрах выпуска сохранена.", "Сохранение приземных параметров.", MessageBoxButtons.OK, MessageBoxIcon.Information);
               

            }
            catch (Exception er)
            {
                MessageBox.Show("Ошибка при записи в файл  InfoDatas.xml" + "|" + er.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }



            //string s = System.AppDomain.CurrentDomain.BaseDirectory + "Result Files\\" + dateTimePicker2.Value.ToString("yyyyMMdd") + "-" + maskedTextBox2.Text.Replace(":", "");
            //ResultFolder = s;
            //if (Directory.Exists(s) == false)
            //{
            //    Directory.CreateDirectory(s);
            //}
            //DatasFileInfo = s + "\\" + dateTimePicker2.Value.ToString("yyyyMMdd") + "-" + maskedTextBox2.Text.Replace(":", "") + ".info";

            //string s = System.AppDomain.CurrentDomain.BaseDirectory;
            //DatasFileInfo = s + "\\start.info";

            //if (ReadZondSettigs() == true)
            //{
            //    StreamWriter swdata = new StreamWriter(DatasFileInfo, false, System.Text.Encoding.GetEncoding("windows-1251"));
            //    swdata.WriteLine("StationSynopticIndex:" + Convert.ToChar(0x09) + StationParametrs.SynopIndex.ToString());
            //    swdata.WriteLine("StationLongitude:" + Convert.ToChar(0x09) + StationParametrs.Longitude.ToString());
            //    swdata.WriteLine("StationLattitude:" + Convert.ToChar(0x09) + StationParametrs.Latitude.ToString());
            //    swdata.WriteLine("StationHeightAboveSeaLevel:" + Convert.ToChar(0x09) + StationParametrs.Altitude.ToString());

            //    swdata.WriteLine("OnGroundPressure:" + Convert.ToChar(0x09) + textBox3.Text);
            //    swdata.WriteLine("OnGroundWindDirection:" + Convert.ToChar(0x09) + textBox1.Text);
            //    swdata.WriteLine("OnGroundWindVelocity:" + Convert.ToChar(0x09) + textBox2.Text);
            //    swdata.WriteLine("OnGroundHumidityError:" + Convert.ToChar(0x09) + textBox6.Text);
            //    swdata.WriteLine("OnGroundTemperatureError:" + Convert.ToChar(0x09) + textBox5.Text);

            //    swdata.WriteLine("StartYear:" + Convert.ToChar(0x09) + dateTimePicker2.Value.ToString("yyyy"));
            //    swdata.WriteLine("StartMonth:" + Convert.ToChar(0x09) + dateTimePicker2.Value.ToString("mm"));
            //    swdata.WriteLine("StartDay:" + Convert.ToChar(0x09) + dateTimePicker2.Value.ToString("dd"));
            //    swdata.WriteLine("StartHour:" + Convert.ToChar(0x09) + maskedTextBox2.Text.Substring(0, 2));
            //    swdata.WriteLine("StartMinute:" + Convert.ToChar(0x09) + maskedTextBox2.Text.Substring(3, 2));

            //    swdata.WriteLine("NebulosityCode:" + Convert.ToChar(0x09) + textBox4.Text);
            //    swdata.WriteLine("RadioZondType:" + Convert.ToChar(0x09) + maskedTextBox1.Text);

            //    swdata.Close();
            //    MessageBox.Show("Инофрмация о приземных параметрах выпуска сохранена.", "Сохранение приземных параметров.", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    label45.Visible = false;
            //}
            
        }


        private void button4_Click(object sender, EventArgs e)
        {

            GetАМКDatas();
        }

        private void StopАМКDatas()
        {

                try
                {
                    Stopflag = false;
                    QML_connection = false;
                    if (t_amk != null)
                    {
                        t_amk.Abort();
                    }
                }
                catch (Exception e)
                {
                }

        }

        private void GetАМКDatas()
  
        {
             if (ReadZondSettigs() == true)
                {

                    t_amk = new Thread(StartGetQMLDataFromTCPIP);
                    t_amk.Start();
                }
        }

        public void StartGetQMLDataFromTCPIP()
        {

            //sw = new StreamWriter("C:\\Program Files (x86)\\LANIT\\QMLAgent\\test" + StartThr + ".txt");

            Stopflag = true;
            while (Stopflag == true)
            {
                try
                {
                    try
                    {
                        client = new TcpClient(AMK_TCPIP.IP_adress, System.Convert.ToInt32(AMK_TCPIP.IP_port));
                        AMKLabel_Status("Связь с АМК установлена");
                    }
                    catch (Exception e)
                    {
                        AMKLabel_Status("Связь с АМК НЕ УСТАНОВЛЕНА");
                    }
             
                    NetworkStream myNetworkStream = client.GetStream();
                    byte[] buffer = new byte[16384];
                    QML_connection = true;

                    TimePreviousRead = DateTime.Now;


                    while (Stopflag == true && QML_connection == true)
                    {
                        if (client.Connected == true)
                        {
                            if (myNetworkStream.CanRead)
                            {

                                if (myNetworkStream.DataAvailable)
                                {
                                    Int32 bytes = myNetworkStream.Read(buffer, 0, buffer.GetLength(0));
                                    responseData = System.Text.Encoding.ASCII.GetString(buffer, 0, bytes);
                                    dataRead = true;

                                        if (responseData.Length > 13)
                                        {
                                            if (responseData.IndexOf("<AirTemp>Temp") != -1)
                                            {
                                                responseQMLString = responseData;
                                                ResponseDataFromQML_QMLString();
                                                AMKLabel_Status("Поступают");
                                            }
                                        }
                                        TimePreviousRead = DateTime.Now;
                    
                                }
                                else
                                {
                                    if (DateTime.Now.Day == TimePreviousRead.Day)
                                        WaitTimeRead = Math.Abs((DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second) - (TimePreviousRead.Hour * 3600 + TimePreviousRead.Minute * 60 + TimePreviousRead.Second));
                                    else
                                        WaitTimeRead = 86400 - Math.Abs((DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second) - (TimePreviousRead.Hour * 3600 + TimePreviousRead.Minute * 60 + TimePreviousRead.Second));

                                    if (WaitTimeRead > 120)
                                    {
                                        QML_connection = false;
                                        responseData = String.Empty;
                                    }
                                    Thread.Sleep(1000);
                                }
                            }
                            else
                            {
                                QML_connection = false;
                            }
                        }
                        else
                        {
                            QML_connection = false;
                        }
                    }

                    client.Close();
                }
                catch (Exception e)
                {
                    responseData = responseData + "Ошибка: " + e.Message;

                }

                // таймер на повторное обращение к порту
                Thread.Sleep(10000);
            }
            //sw.Close();


        }

        private void Addlabel41text(string s)
        {
            if (label41.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(Addlabel41text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label41.Visible = true;
                label41.Text = s;
            }
        }

        private void AddtextBox17text(string s)
        {
            if (textBox17.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddtextBox17text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox17.Text = s;
            }
        }

        private void AddtextBox18text(string s)
        {
            if (textBox18.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddtextBox18text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox18.Text = s;
            }
        }

        private void AddtextBox19text(string s)
        {
            if (textBox19.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddtextBox19text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox19.Text = s;
            }
        }

        private void Addlabel47text(string s)
        {
            if (label47.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(Addlabel47text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label47.Text = s;
            }
        }

        private void Addlabel46text(string s)
        {
            if (label46.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(Addlabel46text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label46.Text = s;
            }
        }

        private void AddtextBox16text(string s)
        {
            if (textBox16.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddtextBox16text);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox16.Text = s;
            }
        }

        private void ZondLabel_Admissible_error_check(string s)
        {
            if (label27.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(ZondLabel_Admissible_error_check);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label27.Text = s;
                label27.Visible = true;
            }
        }


        private void ZondLabel_GPSCheck(string s)
        {
            if (label35.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(ZondLabel_GPSCheck);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label35.Text = s;
                label35.Visible = true;
            }
        }


        private void ZondLabel_GetStart(string s)
        {
            if (label29.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(ZondLabel_GetStart);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label29.Text = s;
                label29.Visible = true;
            }
        }

        private void ZondLabel_Status(string s)
        {
            if (label20.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(ZondLabel_Status);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label20.Text = s;
                label20.Visible = true;
            }
        }

        private void AMKLabel_Status(string s)
        {
            if (label33.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AMKLabel_Status);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                label33.Text = s;
                label33.Visible = true;
            }
        }



        private void AddZondDatas_RSSI(string s)
        {
            if (textBox14.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_RSSI);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox14.Text = s;
            }
        }

         private void AddZondDatas_Altitude(string s)
        {
            if (textBox11.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_Altitude);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox11.Text = s;
            }
        }

         private void AddZondDatas_Longitude(string s)
        {
            if (textBox12.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_Longitude);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox12.Text = s;
            }
        }
         private void AddZondDatas_Latitude(string s)
        {
            if (textBox13.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_Latitude);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox13.Text = s;
            }
        }
        private void AddZondDatas_AirTemp_Temp(string s)
        {
            if (textBox10.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_AirTemp_Temp);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox10.Text = s;
            }
        }

        private void AddZondDatas_Hum_Relative(string s)
        {
            if (textBox9.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddZondDatas_Hum_Relative);
                this.Invoke(setTextDelegate, new object[] { s });
            }
            else
            {
                textBox9.Text = s;
            }
        }


        private void AddAMKDatas_AirTemp_Temp(string QMLString_P)
        {
            if (textBox8.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMKDatas_AirTemp_Temp);
                this.Invoke(setTextDelegate, new object[] { QMLString_P });
            }
            else
            {
                textBox8.Text = QMLString_P;
            }
        }


        private void AddAMKDatas_Hum_Relative(string QMLString_P)
        {
            if (textBox7.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMKDatas_Hum_Relative);
                this.Invoke(setTextDelegate, new object[] { QMLString_P });
            }
            else
            {
                textBox7.Text = QMLString_P;
            }
        }

        private void AddAMKDatas_Pres_StationL(string QMLString_P)
        {
            if (textBox3.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMKDatas_Pres_StationL);
                this.Invoke(setTextDelegate, new object[] { QMLString_P });
            }
            else
            {
                textBox3.Text = QMLString_P;
            }
        }

        private void AddAMKDatas_Wind1_Speed(string QMLString_P)
        {
            if (textBox2.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMKDatas_Wind1_Speed);
                this.Invoke(setTextDelegate, new object[] { QMLString_P });
            }
            else
            {
                textBox2.Text = QMLString_P;
            }
        }

        private void AddAMKDatas_Wind1_Direct(string QMLString_P)
        {
            if (textBox1.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMKDatas_Wind1_Direct);
                this.Invoke(setTextDelegate, new object[] { QMLString_P });
            }
            else
            {
                textBox1.Text = QMLString_P;
            }
        }

        private void AddAMK_Time(string DT)
        {
            if (label16.InvokeRequired)
            {
                SetTextDelegate setTextDelegate = new SetTextDelegate(AddAMK_Time);
                this.Invoke(setTextDelegate, new object[] { DT });
            }
            else
            {
                label16.Text = "Данные АМК " + DT;
                label16.Visible = true;
            }
        }

           public void ResponseDataFromQML_QMLString()
        {

            if ((responseQMLString.Length > 13) && (dataRead == true))
            {
                    dataRead = false;
                    try
                    {
                        QMLString_Pars(responseQMLString);
                        AddAMK_Time(DateTime.Now.ToString("HH:mm"));
               

                        for (int i = 0; i <= QMLStringParam.Length - 1; i++)
                        {
                            if (QMLStringParam[i].N == "AirTemp_Temp")
                            {
                                AddAMKDatas_AirTemp_Temp(QMLStringParam[i].Val);
                            }
                            else if (QMLStringParam[i].N == "Hum_Relative")
                            {
                                AddAMKDatas_Hum_Relative(QMLStringParam[i].Val);
                            }
                            else if (QMLStringParam[i].N == "Pres_StationL")
                            {
                                AddAMKDatas_Pres_StationL(QMLStringParam[i].Val);
                            }
                            else if (QMLStringParam[i].N == "Wind1_Speed")
                            {
                                AddAMKDatas_Wind1_Speed(QMLStringParam[i].Val);
                            }
                            else if (QMLStringParam[i].N == "Wind1_Direct")
                            {
                                AddAMKDatas_Wind1_Direct(QMLStringParam[i].Val);
                            }                            
                        }
                    }
                    catch { }
            } 

        }
           public void QMLString_Pars(string QMLStringResponse)
           {


               int i;
               int paramcount = 0;
               for (i = 0; i <= QMLStringResponse.Length - 1; i++)
               {
                   if (QMLStringResponse[i] == '>')
                   { paramcount++; }
               }
               QMLStringParam = new ParamType[paramcount];

               int k = 0;
               for (i = 0; i <= QMLStringResponse.Length - 1; i++)
               {
                   if (QMLStringResponse[i] == '<')
                   {
                       QMLStringParam[k].N = QMLStringResponse.Substring(QMLStringResponse.IndexOf('<', i) + 1, QMLStringResponse.IndexOf(':', i) - QMLStringResponse.IndexOf('<', i) - 1);
                       QMLStringParam[k].Val = QMLStringResponse.Substring(QMLStringResponse.IndexOf(':', i) + 1, QMLStringResponse.IndexOf(';', i) - QMLStringResponse.IndexOf(':', i) - 1);
                       QMLStringParam[k].N = QMLStringParam[k].N.Replace('>', '_');
                       k++;
                   }
               }
           }

           private void Form1_FormClosed(object sender, FormClosedEventArgs e)
           {
               StopАМКDatas();
           }

           private void textBox8_TextChanged(object sender, EventArgs e)
           {
               different_amk_zond_temp();

               LendedParametrs_check();
           }


           public void different_amk_zond_temp()
           {
               try
               {
                   double dif = Math.Round(System.Convert.ToDouble(textBox8.Text) - System.Convert.ToDouble(textBox10.Text),2);
                   textBox5.Text = dif.ToString();
               }

                 catch 
                 {
                     textBox5.Text = "";
                 }
               
           }

           public void different_amk_zond_hum()
           {
               try
               {
                   double dif = System.Convert.ToDouble(textBox7.Text) - System.Convert.ToDouble(textBox9.Text);
                   textBox6.Text = dif.ToString();
               }

               catch
               {
                   textBox6.Text = "";
               }

           }
           private void textBox10_TextChanged(object sender, EventArgs e)
           {
               different_amk_zond_temp();
           }

           private void textBox7_TextChanged(object sender, EventArgs e)
           {
               different_amk_zond_hum();
               LendedParametrs_check();
           }

           private void textBox9_TextChanged(object sender, EventArgs e)
           {
               different_amk_zond_hum();
           }

           private void button5_Click(object sender, EventArgs e)
           {
               try
               {
                   if (t != null)
                   {
                       t.Abort();
                       t = null;
                   }
               }
               catch
               { }

               testStart = true;
               Lastmessagetime = DateTime.Now;

               label16.Text="";
               label27.Tag = "false";
               label27.Text = "";
               label35.Tag = "false";
               label35.Text = "";
              
               StopАМКDatas();
               GetАМКDatas();
               StopZondDatas();
               GetZondDatas();
               StopCheckThread();
               StartCheckThread();
               LastMessageTimeCheckThreadStop();
               button1.Enabled = false;
               button2.Enabled = false;
               button4.Enabled = true;

               groupBox2.Visible = false;
               groupBox6.Visible = false;
               
           }

           private void StopCheckThread()
           {
               try
               {
                   if (t_check != null)
                   {
                       t_check.Abort();
                       t_check = null;
                   }
               }
               catch
               {
               }
               
           }
                 
              private void StartCheckThread()
           {
               t_check = new Thread(start_check);
               t_check.IsBackground = true;
               t_check.Start();
            }
           
        private void LastMessageTimeCheckThreadStart()
           {
               t_dTime = new Thread(LastMessageTimeCheck);
               t_dTime.IsBackground = true;
               t_dTime.Start();
            }
        private void LastMessageTimeCheckThreadStop()
        {
            try
            {
                if (t_dTime != null)
                {
                    t_dTime.Abort();
                    t_dTime = null;
                }
            }
            catch
            {
            }

        }
            
            
              

           private void StopZondDatas()
           {

               try
               {
                   Zond_serialPort.Close();
               }
               catch (Exception er)
               {
                   StreamWriter writer = null;
                   writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                   writer.WriteLine(DateTime.Now.ToString() + " : Ошибка закрытия порта " + er.Message);
                   writer.Close();
                   MessageBox.Show("Ошибка закрытия порта: " + er.Message, "Ошибка закрытия порта", MessageBoxButtons.OK, MessageBoxIcon.Warning);

               }
               if (t_zond != null)
               {
                   t_zond.Abort();
                   t_zond = null;
               }
 

           }
           private void GetZondDatas()
           {
               ReadZondDemo();

               ZondMessage = new ZondMessageType[1];

               if (ReadZondSettigs() == true)
               {

                   SetZondSettings();

                   try
                   {


                       if (Zond_serialPort.IsOpen == true)
                           Zond_serialPort.Close();


                       //---------
                       t_zond = new Thread(StartListeTest);
                       t_zond.IsBackground = true;
                       t_zond.Start();
                       //--------
                   }
                   catch (Exception er)
                   {
                       label20.Text = "Подключение к приемной станции отсутствует";
                       label20.Visible = true;
                       StreamWriter writer = null;
                       writer = new StreamWriter(FN_LogZond, true, System.Text.Encoding.GetEncoding("windows-1251"));
                       writer.WriteLine(DateTime.Now.ToString() + " : Ошибка подключения " + er.Message);
                       writer.Close();
                       MessageBox.Show("Ошибка подключения: " + er.Message, "Ошибка подключения", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                   }
               }
           }
           private void label23_Click(object sender, EventArgs e)
           {

           }

           private void label19_Click(object sender, EventArgs e)
           {

           }

           private void stop_test_click()
           {
               testStart = false;
               try
               {
                   Zond_serialPort.Close();
               }
               catch (Exception er)
               {
               }
               try
               {
                   if (t != null)
                   {
                       t.Abort();
                       t = null;
                   }
               }
               catch (Exception er)
               {
               }

               button1.Enabled = true;
               button2.Enabled = false;


               label20.Text = "";
               label22.Text = "";
               label27.Text = "";
               label33.Text = "";
               label29.Text = "";
               label31.Text = "";
               label35.Text = "";
               
               

               StopАМКDatas();
               StopZondDatas();
               StopCheckThread();
               
           }
           private void button4_Click_1(object sender, EventArgs e)
           {
               stop_test_click();
           }

           private void Admissible_error_check()
           {

               try
               {
                   if ((Math.Abs(System.Convert.ToDouble(textBox5.Text)) > System.Convert.ToDouble(Admissible_error.temp))
                       ||
                       (Math.Abs(System.Convert.ToDouble(textBox6.Text)) > System.Convert.ToDouble(Admissible_error.hum)))
                   {
                       label27.Tag = "false";
                       ZondLabel_Admissible_error_check("Не в предлелах допуска!");
                       label27.ForeColor = Color.Red;
                   }
                   else
                   {
                       label27.Tag = "true";
                       ZondLabel_Admissible_error_check("В предлелах допуска");
                       label27.ForeColor = Color.DarkGreen;
                   }
               }
               catch
               {
                   label27.Tag = "false";
                   ZondLabel_Admissible_error_check("Не в предлелах допуска!");
                   label27.ForeColor = Color.Red;
               }

           }

           private void GPS_check()
           {
               
               
               
               try
               {
                   double  a=System.Convert.ToDouble(textBox11.Text);
                   double lon = System.Convert.ToDouble(textBox12.Text);
                   double lat = System.Convert.ToDouble(textBox13.Text);

                   if ((a > 0 && a <35000) && (lon >20 && lon <180) && (lat >45 && lat <80))

                   {
                       
                       label35.Tag = "true";
                       ZondLabel_GPSCheck("Поступают");
                       
                   }
                   else
                   {
                       label35.Tag = "false";
                       ZondLabel_GPSCheck("Не поступают !");
                   }
               }
               catch
               {
                   label35.Tag = "false";
                   ZondLabel_GPSCheck("Не поступают !");  

               }
           }

           private void start_check()
           {
               while (true)
               {
                   GPS_check();
                   Admissible_error_check();
                   if (label35.Tag == (object)"true" && label27.Tag == (object)"true")
                   {
                       ZondLabel_GetStart("Готов");
                       label29.ForeColor = Color.DarkGreen;

                   }
                   else
                   {
                       ZondLabel_GetStart("Не готов");
                       label29.ForeColor = Color.Red;

                   }

                   //проверка на то что подключен зонд к станции или нет


                   if ((DateTime.Now.Subtract(Lastmessagetime).Ticks / 1000) > 10000)
                   {
                       if (((ZondMessage[0].Humidity < (double)0) || (ZondMessage[0].Humidity > (double)100)) || ( (ZondMessage[0].Temperature > (double)50) || ( ZondMessage[0].Temperature < (double)-70.0)))
                       {
                           if (com_connection == true)
                           {
                               StopАМКDatas();
                               StopZondDatas();
                               
                              // MessageBox.Show(ZondMessage[0].Humidity.ToString(), "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                               MessageBox.Show("К приемной станции не подключен зонд \r\nили к зонду не подключен датчик температуры и влажности! \r\n" + ZondMessage[0].Humidity, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                               StopCheckThread();
                               // stop_test_click();
                           }
                       }
                   }

                   

                   Thread.Sleep(1000);

               }
               
           }

           private void LastMessageTimeCheck()
           {
               while (true)
               {
                   if (Lastmessagetime.Year != 2000)
                   {
                    delta_Lastmessagetime = DateTime.Now - Lastmessagetime;
                    //delta_Lastmessagetime = time_now - Lastmessagetime;
                    
                    //int dt = (DateTime.Now.Hour * 60 + DateTime.Now.Minute * 60 + DateTime.Now.Millisecond) - (Lastmessagetime.Hour * 60 + Lastmessagetime.Minute * 60 + Lastmessagetime.Millisecond);
                    if (delta_Lastmessagetime.TotalSeconds > 1)
                        AddtextBox18text(Math.Floor(delta_Lastmessagetime.TotalSeconds).ToString());
                    else
                        AddtextBox18text(Math.Round(delta_Lastmessagetime.TotalSeconds).ToString());


                   }

                   Thread.Sleep(1000);

               }
               
           }

        

           private void textBox5_TextChanged(object sender, EventArgs e)
           {
               LendedParametrs_check();
           }

           private void textBox6_TextChanged(object sender, EventArgs e)
           {
               LendedParametrs_check();
           }

           private void textBox11_TextChanged(object sender, EventArgs e)
           {
         
           }

           private void textBox12_TextChanged(object sender, EventArgs e)
           {
              
           }

           private void textBox13_TextChanged(object sender, EventArgs e)
           {
               
           }

           private void label9_Click(object sender, EventArgs e)
           {

           }

           private void label10_Click(object sender, EventArgs e)
           {

           }

           private void label35_TextChanged(object sender, EventArgs e)
           {
             //  start_check();
           }

           private void label27_TextChanged(object sender, EventArgs e)
           {
            //   start_check();
           }

           private void groupBox15_Enter(object sender, EventArgs e)
           {

           }

           private void textBox16_TextChanged(object sender, EventArgs e)
           {

           }
          
           
        private void UpOrDown_check()
           {

               if ((Altitude3point[0] >= Altitude3point[1]) && (Altitude3point[1] >= Altitude3point[2])
                   && Altitude3point[0] != 0 && Altitude3point[1] != 0 && Altitude3point[2] != 0)
               {
                   label41.ForeColor =  Color.DarkGreen;
                   Addlabel41text("Зонд поднимается");
               }
               else if ( Altitude3point[0] != 0 && Altitude3point[1] != 0 && Altitude3point[2] != 0)
               {
                   label41.ForeColor = Color.Red;
                   Addlabel41text("Зонд падает");
               }

                              
           }

        private void LendedParametrs_check()
        {
            if (//maskedTextBox1.Text != ""
                textBox1.Text != ""
                && textBox2.Text != ""
                && textBox3.Text != ""
                //&& textBox4.Text != ""
                && textBox5.Text != ""
                && textBox6.Text != ""
                && textBox7.Text != ""
                && textBox8.Text != "")
            {
                button3.Enabled = true;
                label42.Visible = false;
            }
            else
            {
                button3.Enabled = false;
                label42.Visible = true;
            }


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            LendedParametrs_check();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            LendedParametrs_check();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            LendedParametrs_check();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            LendedParametrs_check();
        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
