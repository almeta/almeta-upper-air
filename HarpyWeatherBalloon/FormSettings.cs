﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Xml;
using System.Xml.Linq;

using System.Globalization;

using System.IO;
using System.IO.Ports;

namespace HarpyWeatherBalloon
{
    public partial class FormSettings : Form
    {


        Form1.StationParametrsType StationParametrs;

        Form1.SerialPostSettings ZondSerialPort;
        Form1.AMK_SettingsType AMK_TCPIP;
        Form1.SendDatasParametrsType SendDatasParametrs;
        Form1.Admissible_errorType Admissible_error;

        string filesettingspath;

        string zond_lat, zond_lon;


        public FormSettings()
        {
            InitializeComponent();
        }


        public bool ShowFormSettings(Form1.StationParametrsType S_Parametrs, Form1.SerialPostSettings Z_SerialPort, Form1.SendDatasParametrsType Z_SendDatas, Form1.AMK_SettingsType AMK_s, Form1.Admissible_errorType Adm_err_s, string fsettingspath, string Lon_str, string Lat_str)
        {
            bool res = false;
            StationParametrs = S_Parametrs;
            ZondSerialPort = Z_SerialPort;
            SendDatasParametrs = Z_SendDatas;
            filesettingspath = fsettingspath;
            zond_lon = Lon_str;
            zond_lat = Lat_str;
            AMK_TCPIP = AMK_s;
            Admissible_error = Adm_err_s;

            if (this.ShowDialog() != DialogResult.OK)
            {
                return res;
            }
            res = true;
            return res;
        }

        private void FormSettings_Load(object sender, EventArgs e)
        {
            textBox8.Text = StationParametrs.StationName;
            textBox6.Text = StationParametrs.SynopIndex.ToString();
            textBox5.Text = StationParametrs.Latitude.ToString();
            textBox4.Text = StationParametrs.Longitude.ToString();
            textBox3.Text = StationParametrs.Altitude.ToString();

            

            comboBox1.Text = ZondSerialPort.PortName;
            comboBox2.Text = ZondSerialPort.BaudRate.ToString();
            comboBox3.Text = ZondSerialPort.DataBits.ToString();

            textBox2.Text = ZondSerialPort.ZondPeriodSend.ToString();

            textBox9.Text = SendDatasParametrs.URL;
            if (SendDatasParametrs.SendDatas == true)
                checkBox1.Checked = true;
            else
                checkBox1.Checked = false;
            try
            {
                textBox1.Text = AMK_TCPIP.IP_adress.ToString();
                textBox7.Text = AMK_TCPIP.IP_port.ToString();
            }
            catch 
            {
            }

            try
            {
                textBox11.Text = Admissible_error.temp.ToString();
                textBox10.Text = Admissible_error.hum.ToString();
            }
            catch 
            {
            }

            string[] ports = SerialPort.GetPortNames();

            comboBox1.Items.Clear();

            for (int i = 0; i < ports.Count(); i++)
            {
                comboBox1.Items.Add(ports[i]);
            }




            }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ZondSerialPort.PortName = comboBox1.Text;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            ZondSerialPort.BaudRate = Convert.ToInt32(comboBox2.Text);
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            ZondSerialPort.DataBits = Convert.ToInt32(comboBox3.Text);
        }

        private void button1_Click(object sender, EventArgs e)
        {
               try
            {

            string ds = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

            AMK_TCPIP.IP_adress= textBox1.Text;
            AMK_TCPIP.IP_port = textBox7.Text;

            StationParametrs.StationName = textBox8.Text;
            StationParametrs.SynopIndex = Convert.ToInt32(textBox6.Text);
            string s = textBox5.Text;
            if (ds == ".")
                s = s.Replace(",", ".");
            else if (ds == ",")
                s = s.Replace(".", ",");
            StationParametrs.Latitude = Convert.ToDouble(s);

            s = textBox4.Text;
            if (ds == ".")
                s = s.Replace(",", ".");
            else if (ds == ",")
                s = s.Replace(".", ",");
            StationParametrs.Longitude = Convert.ToDouble(s);

            s = textBox3.Text;
            if (ds == ".")
                s = s.Replace(",", ".");
            else if (ds == ",")
                s = s.Replace(".", ",");            
            StationParametrs.Altitude = Convert.ToDouble(s);

            s = textBox11.Text;
            if (ds == ".")
                s = s.Replace(",", ".");
            else if (ds == ",")
                s = s.Replace(".", ",");
            Admissible_error.temp =(s);

            s = textBox10.Text;
            if (ds == ".")
                s = s.Replace(",", ".");
            else if (ds == ",")
                s = s.Replace(".", ",");
            Admissible_error.hum = (s);

                // Создаем экземпляр класса
             //   XDocument xmlSettingsDoc = XDocument.Load(filesettingspath);

                //XDocument xmlSettingsDoc = new XDocument("Driver_Settings");
                XElement xmlSettingsDoc = new XElement("Driver_Settings");

                XElement xmlSerialPortSettings = new XElement("SerialPort_Settings",
                    new XElement("PortName"), new XElement("StopBits"), new XElement("BaudRate"), new XElement("ZondPeriodSend"));
                xmlSettingsDoc.Add(xmlSerialPortSettings); 
                xmlSettingsDoc.Element("SerialPort_Settings").Element("PortName").Value = ZondSerialPort.PortName;
                xmlSettingsDoc.Element("SerialPort_Settings").Element("StopBits").Value = ZondSerialPort.StopBits.ToString();
                xmlSettingsDoc.Element("SerialPort_Settings").Element("BaudRate").Value = ZondSerialPort.BaudRate.ToString();
                xmlSettingsDoc.Element("SerialPort_Settings").Element("ZondPeriodSend").Value = ZondSerialPort.ZondPeriodSend.ToString();



                XElement xmlStationSettings = new XElement("Station_Settings",
                    new XElement("StationName"), new XElement("SynopIndex"), new XElement("Longitude"), new XElement("Latitude"), new XElement("Altitude"));
                xmlSettingsDoc.Add(xmlStationSettings);
                xmlSettingsDoc.Element("Station_Settings").Element("StationName").Value = StationParametrs.StationName.ToString();
                xmlSettingsDoc.Element("Station_Settings").Element("SynopIndex").Value = StationParametrs.SynopIndex.ToString();
                xmlSettingsDoc.Element("Station_Settings").Element("Longitude").Value = StationParametrs.Longitude.ToString();
                xmlSettingsDoc.Element("Station_Settings").Element("Latitude").Value = StationParametrs.Latitude.ToString();
                xmlSettingsDoc.Element("Station_Settings").Element("Altitude").Value = StationParametrs.Altitude.ToString();
               
                XElement xmlSendDatas = new XElement("SendDatas_Settings",
                    new XElement("SendToURL"),new XElement("URL"));
                xmlSettingsDoc.Add(xmlSendDatas);
                xmlSettingsDoc.Element("SendDatas_Settings").Element("SendToURL").Value = SendDatasParametrs.SendDatas.ToString();
                
                xmlSettingsDoc.Element("SendDatas_Settings").Element("URL").Value = SendDatasParametrs.URL.ToString();


               XElement xmlАМК_Settings = new XElement("АМК_Settings",
                    new XElement("ip_adress"),new XElement("ip_port"));
                xmlSettingsDoc.Add(xmlАМК_Settings);
                xmlSettingsDoc.Element("АМК_Settings").Element("ip_adress").Value = AMK_TCPIP.IP_adress.ToString();
                xmlSettingsDoc.Element("АМК_Settings").Element("ip_port").Value = AMK_TCPIP.IP_port.ToString();

                if (Admissible_error.temp != "" && Admissible_error.hum != "")
                {
                    XElement xmlAdmissible_error = new XElement("Admissible_error",
                         new XElement("temperature"), new XElement("humidity"));
                    xmlSettingsDoc.Add(xmlAdmissible_error);
                    xmlSettingsDoc.Element("Admissible_error").Element("temperature").Value = Admissible_error.temp.ToString();
                    xmlSettingsDoc.Element("Admissible_error").Element("humidity").Value = Admissible_error.hum.ToString();
                }


                StreamWriter xmlSettingsWr = new StreamWriter(filesettingspath);
                xmlSettingsWr.Write(xmlSettingsDoc);
                xmlSettingsWr.Close();

            }
            catch (Exception er)
            {
                MessageBox.Show("Ошибка при записи в файл настроек (Zond_Settings.xml)" + "|" + er.Message, "Ошибка!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            this.DialogResult = DialogResult.OK;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            ZondSerialPort.ZondPeriodSend= Convert.ToInt32(textBox2.Text);
        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {
            SendDatasParametrs.URL =  textBox9.Text.Trim();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
                SendDatasParametrs.SendDatas = true;
            if (checkBox1.Checked == false)
                SendDatasParametrs.SendDatas = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox5.Text = zond_lat;
            textBox4.Text = zond_lon;


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            AMK_TCPIP.IP_adress = textBox1.Text.Trim();
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            AMK_TCPIP.IP_port = textBox7.Text.Trim();
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {
       
        }



    }
}
