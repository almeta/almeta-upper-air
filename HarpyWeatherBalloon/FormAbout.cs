﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HarpyWeatherBalloon
{
    public partial class FormAbout : Form
    {
        public FormAbout()
        {
            InitializeComponent();
        }

        public bool ShowFormAbout()
        {
            bool res = false;

            if (this.ShowDialog() != DialogResult.OK)
            {
                return res;
            }
            res = true;
            return res;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }



        private void FormAbout_Load_1(object sender, EventArgs e)
        {
            String strVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            label2.Text = "Версия : " + strVersion;
        }
    }
}
